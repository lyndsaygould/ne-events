<?php
    require_once ('functions.php');
    echo getHeader("Reports");
?>


<section class="wrapper">

    <!-- table where all events are listed -->


    
<?php

    // ------- CHECK IF IT IS ADMIN -----------

    if (isset($_SESSION['logged-in'])) {
        if ($_SESSION['logged-in']) { // if it is true
            if ($_SESSION['uName'] == 'nick')
                echo "<h1>Reports</h1>\n"; // welcoming message
				echo "<p class=\"center\">Comments user's have reported as inappropriate.</p>";
        }
    } else {
        header("Location: index.php"); // redirects to homepage
        exit; // exits
    }

        // ------- DISPLAY THE REPORTS -----------

    include 'database_conn.php'; // makes a db connection

    $rID = isset($_REQUEST['rIDnotChecked']) ? $_REQUEST['rIDnotChecked'] : null;
    $reID = isset($_REQUEST['rIDchecked']) ? $_REQUEST['rIDchecked'] : null;
    $eID = isset($_REQUEST['eventID']) ? $_REQUEST['eventID'] : null;

    if (!empty($rID))
    {

        $sqlUpdate = "UPDATE cte_reports
                SET status='Checked'
                WHERE reportID='$rID'";
        $uUpdate = mysqli_query($conn, $sqlUpdate) or die (mysqli_error($conn)); // run the query or die if there is an error
        mysqli_query($conn, $sqlUpdate) or die(mysqli_error($conn));
    }

    if (!empty($reID))
    {

        $sqlUpdateStatus = "UPDATE cte_reports
                    SET status='Not checked'
                    WHERE reportID='$reID'";
        $rUpdate = mysqli_query($conn, $sqlUpdateStatus) or die (mysqli_error($conn)); // run the query or die if there is an error
        mysqli_query($conn, $sqlUpdateStatus) or die(mysqli_error($conn));
    }

    $sql = "SELECT cte_events.eventTitle, cte_users.firstName, cte_users.surName, cte_feedbacks.feedback, cte_feedbacks.feedbackID, cte_reports.report, cte_reports.date,
            cte_reports.status, cte_reports.reportID
            FROM cte_users JOIN cte_reports ON cte_users.userID = cte_reports.uID 
            JOIN cte_feedbacks ON cte_feedbacks.feedbackID = cte_reports.feedbackID  JOIN cte_events ON cte_events.eventID = cte_feedbacks.eID 
            ORDER BY cte_reports.date ASC";

    $rEvents = mysqli_query($conn, $sql) or die (mysqli_error($conn)); // run the query or die if there is an error

    if (mysqli_num_rows($rEvents) != 0) {

        echo "<table>
                    <tr>
                    <th>Name</th>
                    <th>Event</th>
                    <th>Feedback</th>
                    <th>Report</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Delete this feedback</th>
                    <th>Mark as checked</th>     
                    </tr>";

    while ($row = mysqli_fetch_assoc($rEvents)) { // loop to retrieve needed data

        $reportID = $row['reportID'];
        $userFname = $row['firstName']; // gets event ID
        $userSname = $row['surName'];
        $feedback = $row['feedback'];
        $fID = $row['feedbackID'];
        $report = $row['report'];
        $date = $row['date'];
        $status = $row['status'];
        $eventTitle = $row['eventTitle'];


            echo "  <tr>
                    <td>$userFname $userSname</td>
                    <td>$eventTitle</td>
					<td>$feedback</td> <!-- event ID (eID) is used as PK that will be used in 'selectedEvent' page-->
					<td>$report</td>
					<td>$date</td>
					<td>$status</td>
					<td><a href='deleteFeedback.php?fID=$fID' onclick=\"return confirm('Are you sure you want to delete this feedback');\">Delete</a></td>";
            if ($status == 'Not checked') {
                echo "<td><a href='reports.php?rIDnotChecked=$reportID' >Check</a></td>
			            </tr>";
            }
            if ($status == 'Checked') {
                echo "<td><a href='reports.php?rIDchecked=$reportID'>Uncheck</a></td>
                        </tr> ";
            }



    }
        echo "</table>";
    }
    mysqli_free_result($rEvents); // frees the memory associated with a result
    mysqli_close($conn); // closes the database

?>

    <!-- table -->
</section>
	<div class="clear"></div>
<br />

	<?php echo getFooter();?>