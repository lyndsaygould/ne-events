<?php
    require_once ('functions.php');
    echo getHeader("Delete Feedback");
?>		
<section class="wrapper">
<h1>Delete Feedback</h1>

    <?php

    if (isset($_SESSION['logged-in'])) { //checking if session set to logged in
        if ($_SESSION['logged-in']) { // if it is logged in allow them to access this page
        }
    } else {
        header("Location: index.php"); // if they aren't logged in redirect to home page
        exit;
    }

    include 'database_conn.php';	  // make db connection

    $fID = isset($_REQUEST['fID']) ? $_REQUEST['fID'] : null;

    $sqlSet = "SET foreign_key_checks = 0";
    $rSet = mysqli_query($conn, $sqlSet) or die (mysqli_error($conn)); // run the query or die if there is an error

    $sqlDelete = "DELETE FROM cte_feedbacks WHERE feedbackID='$fID'";

    if (mysqli_query($conn, $sqlDelete)){
        echo "<p class=\"center\">Your feedback has now been deleted</p>";
    }else {
        echo "<p class=\"center\">There was an error!</p>";
    }

    mysqli_query($conn, $sqlDelete) or die(mysqli_error($conn));


    $sqlSecondSet = "SET foreign_key_checks = 1";
    $rSecondSet = mysqli_query($conn, $sqlSecondSet) or die (mysqli_error($conn)); // run the query or die if there is an error

    mysqli_close($conn);
    ?>

</section>
<div class="clear"></div>
<br />

	<?php echo getFooter();?>