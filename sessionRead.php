<?php
    require_once ('functions.php');
    echo getHeader("Session Read");
?>

<?php

    if (isset($_SESSION['uName'])) {
        $username = $_SESSION['uName'];
        echo "<p>Username: $username</p>\n";
    } elseif (!empty($_SESSION['errors'])) {
        $theErrors = $_SESSION['errors'];
        echo "<p><em>The following problem(s), when you tired to login are :</em></p>\n";
        for ($a = 0; $a < count($theErrors); $a++) {
            echo "$theErrors[$a] <br />\n";
        }
    } else {
        echo "<p>You need to login.</p>\n";
    }


?>

</body>
</html>