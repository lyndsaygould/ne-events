<?php
    require_once ('functions.php');
    echo getHeader("Delete Events");

include 'database_conn.php';	  // make db connection

if (isset($_SESSION['logged-in'])) { //checking if session set to logged in
    if ($_SESSION['logged-in']) { // if it is logged in allow them to access this page
    }
} else {
    header("Location: index.php"); // if they aren't logged in redirect to home page
    exit;
}

$eID = isset($_REQUEST['eventID']) ? $_REQUEST['eventID'] : null;

$sqlSet = "SET foreign_key_checks = 0";
$rSet = mysqli_query($conn, $sqlSet) or die (mysqli_error($conn)); // run the query or die if there is an error

$sqlDelete = "DELETE FROM cte_events WHERE eventID='$eID'";

if (mysqli_query($conn, $sqlDelete)){
    echo "<p> Your event has now been deleted</p> <br>";
}else {
    echo "oops!";
}

echo "<a href='manageEvents.php'> Go back to event list </a>";

$sqlSecondSet = "SET foreign_key_checks = 1";
$rSecondSet = mysqli_query($conn, $sqlSecondSet) or die (mysqli_error($conn)); // run the query or die if there is an error

mysqli_query($conn, $sqlDelete) or die(mysqli_error($conn));
mysqli_close($conn);
?>

<body/>
</html>