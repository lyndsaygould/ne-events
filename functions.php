<?php
function getHeader($pagetitle)
{


ini_set("session.save_path", "/home/unn_w14035880/public_html/cte-DESIGN/sessionData");
session_start(); // checks the status of the session

echo"

<!DOCTYPE html>

	<head>
	<meta charset=\"UTF-8\">
		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>
		
		<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">
		<link href=\"https://fonts.googleapis.com/css?family=Kaushan+Script|Quicksand|Montserrat:300,400|Work+Sans\" rel=\"stylesheet\">
		<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
		
		<title>$pagetitle</title>
	</head>

	<body>
	
	<div id=\"container\">
<header>

     <div id=\"login\">";
    if (isset($_SESSION['uName'])) { // if the user logs in
        $username = $_SESSION['uName'];
        echo "Welcome, $username! <a href='logOut.php'>Logout?</a>"; // displays a welcome message and a link to log out
    } else { // if the user is not logged in
        echo '<form method="post" action="UserLoggedIn.php"> 
                    Username:  <input type="text" name="userName"> <!-- username -->
                    Password:  <input type="password" name="pwd"> <!-- password -->
                    <input type="submit" class="button" value="Login">
                </form>'; // displays the login form

        if (!empty($_SESSION['errors'])) { // if the user tried to login but there are some errors
            $theErrors = $_SESSION['errors'];
            for ($a = 0; $a < count($theErrors); $a++) { // using this 'for' loop, error messages will be displayed
                echo "$theErrors[$a] <br />\n";
            }
        }
    }
    echo "
	</div>
	
	
	<div id='test'>
	<a href=\" ./\"><img src=\"images/logo.png\" class=\"logo2\"></a>
    </div>
   
    <div class=\"wrapper\">			
        <nav class='wrapper2'>
            <ul class=\"topnav\" id=\"myTopnav\">
            <div class=\"wrapper3\">
				<a href=\" ./\"><img src=\"images/logo.png\" class=\"logo\"></a>
			</div>
                <li><a href=\"index.php\">Home</a></li>
                <li><a href=\"about.php\">About</a></li>
                <li><a href=\"events.php\">Events</a></li>
                <li><a href=\"contact.php\">Contact Us</a></li>
	
				";
            if (isset($_SESSION['uName'])) {
                $username = $_SESSION['uName'];
                if ($username != 'nick') {
                    echo "<li><a href=\"myEvents.php\">My Events</a></li>";
					echo "<li><a class=\"hide-pc\" href=\"logOut.php\">Log Out</a></li>";
                } else if ($username == 'nick') {
                    echo "<li><a href=\"manageEvents.php\">Manage Events</a></li>";
                    echo "<li><a href=\"reports.php\">Reports</a></li>";
					echo "<li><a class=\"hide-pc\" href=\"logOut.php\">Log Out</a></li>";
                } 
            } else {
                echo "<li><a class='hide-pc' href='login.php'>Login</a></li>";
            }
			
        echo "    
        
        <li class=\"icon\">
                    <a id=\"mobileonly\">Menu</a>
                    <a href=\"javascript:void(0);\" style=\"font-size:15px;\" onclick=\"myFunction()\">☰</a>
                </li>
        
             
            </ul>
            <script>
                function myFunction() {
                    var x = document.getElementById(\"myTopnav\");
                    if (x.className === \"topnav\") {
                        x.className += \" responsive\";
                    } else {
                        x.className = \"topnav\";
                    }
                }
            </script>
        </nav>
    </div>
</header> 
<div id=\"main-content\">";
}

function getFooter()
{
$footerContent = <<<HEADSTART
</div>
<footer>
		<div class="wrapper margin-top-two">
		
	
		<div class="grid">
			<div class="footer-row hide-footer">
			<h1>Contact US</h1>
				<p>Address: Clayton Street, Newcastle, NE1 4PJ <br/>
				Email: hello@neevents.co.uk <br/> 
				Telephone: 0191 251 6666</p>
				
				<h1><a href="privacypolicy.php">PRIVACY</a> / <a href="FAQ.php">FAQ</a></h1>
				
				
			</div>
			
			<div id="social">
					<a href="http://twitter.com" target="_blank"><img class="social-icon" alt="twitter" src="images/twit.png"></a>    
            
                    <a href="http://instagram.com" target="_blank"><img class="social-icon" alt="instagram" src="images/insta.png" ></a> 
                  
                    <a href="http://facebook.com" target="_blank"><img class="social-icon" alt="facebook" src="images/face.png" ></a>

			</div>
		</div>
								<div class="clear"></div>
								<p>&copy; NE Events</p>
			</div>
			
		</footer>
		</div>
	</body> 
</html>
HEADSTART;
	$footerContent .="\n";
	return $footerContent;
}

//http://cwg.usu.edu/images/site/facebook-icon2.png
//http://repositorioacademico.upc.edu.pe/upc/img/tw.png
//http://cwg.usu.edu/images/site/instagramIcon.png