<?php
require_once ('functions.php');
echo getHeader("About NE Events");


echo "<section class=\"wrapper margin-top-two\">

	<aside>
    <div class=\"sidesearch\">
	<h3>Search Events</h3>\n"; // welcoming message

?>




    <form id="searchEvents" action="searchEvents.php" method="get">

        <input maxlength="256" name="searchTitle" type="search" placeholder="Search..." /><br>
        <!-- selecting date -->
        <input type="date" name="date" id="date" /><br>


        <select name="selectVenue"> <!-- selecting venue: user chooses a venue, and value (how it is named in the database) is sent -->
            <option value="">Venue</option>
            <?php
            // function connecting to the db
            include 'database_conn.php';

            $sqlVenue = "SELECT DISTINCT venueName FROM cte_events ORDER BY venueName";
            $rVenue = mysqli_query($conn, $sqlVenue) or die(mysqli_error($conn));

            while ($row = mysqli_fetch_assoc($rVenue)) { // loop to retrieve needed data
                echo "<option value=\"{$row['venueName']}\">";
                echo $row['venueName'];
                echo "</option>\n";
            }
            mysqli_free_result($rVenue); // frees the memory associated with a result
            ?>
        </select><br>


        <select name="location"> <!-- selecting venue: user chooses a venue, and value (how it is named in the database) is sent -->
            <option value="">Location</option>
            <?php
            // function connecting to the db
            include 'database_conn.php';

            $sqlLocation = "SELECT DISTINCT location FROM cte_events ORDER BY location";
            $rLocation = mysqli_query($conn, $sqlLocation) or die(mysqli_error($conn));

            while ($row = mysqli_fetch_assoc($rLocation)) { // loop to retrieve needed data
                echo "<option value=\"{$row['location']}\">";
                echo $row['location'];
                echo "</option>\n";
            }
            mysqli_free_result($rLocation); // frees the memory associated with a result
            ?>
        </select><br>
        <!-- typing price -->
        <input name="selectPrice" type="search" placeholder="Price" /><br>
        <input type="submit" class="purple-button-side" value="Filter" /> <!-- submits the results -->
    </form>
    </div>
    </aside>

    <div id="page-content">
        <h2>Search Results</h2>
        <?php

        // function connecting to the db
        include 'database_conn.php';

        // gets all search results
        $title = isset ($_REQUEST['searchTitle']) ?  $_REQUEST['searchTitle'] : null;
        $date = isset ($_REQUEST['date']) ?  $_REQUEST['date'] : null;
        $venueName = isset ($_REQUEST['selectVenue']) ?  $_REQUEST['selectVenue'] : null;
        $location = isset ($_REQUEST['location']) ?  $_REQUEST['location'] : null;
        $price = isset ($_REQUEST['selectPrice']) ?  $_REQUEST['selectPrice'] : null;

        $sqlEvents = "SELECT cte_events.eventID, cte_events.eventTitle, cte_events.venueName, LEFT(cte_events.eventDesc, 150),
        cte_events.location, cte_events.eventDate, cte_events.eventPrice, cte_events.eventImage, cte_events.numberOfTickets
        FROM cte_events WHERE 1"; // retrieves all data ('WHERE 1') from three tables - te_events, te_category, te_venue by joining them

        $sqlCondition = ''; // variable that contains additional SQL conditions if there are any

        IF (!empty($title)){ // if title is not empty
            $sqlCondition = $sqlCondition . " AND (cte_events.eventTitle LIKE '%$title%') OR (cte_events.location LIKE '%$title%')
			OR (cte_events.venueName LIKE '%$title%')"; // select data where title contains set value
        }
        IF (!empty($date)){ // if this variable is not empty
            $sqlCondition = $sqlCondition . " AND cte_events.eventDate >= '$date'"; // select data where start date is more or equal to that
        }
        IF (!empty($venueName)){ // if this variable is not empty
            $sqlCondition = $sqlCondition . " AND cte_events.venueName = '$venueName'"; // select data where end date is less or equal to that
        }
        IF (!empty($location)){ // if location is not empty
            $sqlCondition = $sqlCondition . " AND cte_events.location = '$location'"; // select data where  location is the same
        }
        IF (!empty($price)) { // if price is not empty
            $sqlCondition = $sqlCondition . " AND cte_events.eventPrice <= '$price'"; // select data where price is less than set value
        }

        $sqlSearch = $sqlEvents . $sqlCondition; // putting SQL search conditions all together

        $rEvents = mysqli_query($conn, $sqlSearch) or die(mysqli_error($conn)); // run the query or die if there is an error

       if (mysqli_num_rows($rEvents) ==0) {
           echo "<br/><br/><br/><br/><h1> NO EVENTS MATCH YOUR SEARCH</h1>";

       } else {
           while ($row = mysqli_fetch_assoc($rEvents)) { // loop to retrieve data


               $eID = $row['eventID']; // gets event ID
               $eTitle = $row['eventTitle'];
               $eDate = $row['eventDate'];
               $loc = $row['location'];
               $eTickets = $row['numberOfTickets'];
               $path = $row['eventImage'];
               $eDesc = $row['LEFT(cte_events.eventDesc, 150)'];
               $venue = $row['venueName'];


               echo "
					
                    <div class=\"upcoming-events-box\"><div class=\"padding-1\">
                    <a href=\"selectedEvent.php?eventID=$eID\">
                        <div class=\"events-img-wrap\"><img src='media/$path'>
                        <p class=\"event-date\">$eDate</p>
                        </div>
                    <div class=\"upcoming-events-box-text\">
                    <span class=\"events-title\"><h1>$eTitle <!-- event ID (eID) is used as PK that will be used in 'selectedEvent' page--></h1></span>
                        <div class=\"clear\"></div>
                    <p class=\"location\">$loc / $venue</p>
                    <p>";

               if (strlen($eDesc) > 149) {
                   echo "$eDesc...";
               } else {
                   echo "$eDesc";
               }
               echo "</p>";

               if ($eTickets == 0) {
                   echo "<img class=\"sold-out\" src=\"images/sold-out.png\">
                                        <p class=\"tickets-left\">Sold out!<br /></p>
                                        <p class=\"hide-pc\">Sold out!</p></a>";
               } else {
                   echo "<p class=\"tickets-left\">Tickets left: $eTickets</p>
                                        <p class=\"hide-pc\">Tickets left: $eTickets</p></a>";
               }
               echo "</p></div>
                                    <div class=\"purple-button\"><a href=\"selectedEvent.php?eventID=$eID\">More information &#10140;</a></div>
                                    <p class=\"mobile-info\"><a class=\"hide-pc\" href=\"selectedEvent.php?eventID=$eID\">More Information...</a></p>
                        <div class=\"clear\"></div>
                        </div></div>
                         ";

           }
           mysqli_close($conn);
       }

?>
    </div>
    </section>
    <div class="clear"></div>
    <br />
<?php echo getFooter();?>