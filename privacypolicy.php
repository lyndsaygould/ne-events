<?php
    require_once ('functions.php');
    echo getHeader("Manage Events");
	?>

<section class="wrapper">
<h1>Disclaimer and Privacy Policy</h1>
    <br/> <br/>
    <div id="privacy">
<p>The information contained on this website is provided by NE Events and Conferences, and whilst we aspire to keep all information up-to-date and accurate,
    we make no warranties of any kind about the accuracy or availability of the events. Any reliance you place on such information is at your own risk. NE Events and
    Conferences does not accept responsibility and excludes liability permitted by law for any loss or damage to any personal property during an event and/or conference.
    NE Events and Conferences is committed to ensuring the protection and security of your privacy. Any information you are asked to provide us with will only be used in accordance
    with this privacy statement. </p>
        <p> We require the information collected to provide you with a high-class service enabling you to securely purchase tickets to events. The information given
    to us by you is used for record keeping and will not be shared with outside companies or personnel within NE Events and Conferences without approved access unless required by law. The
    information you provide us with is used for record keeping and to sell you the tickets you requested to by on the website. By making a purchase from NE Events and Conferences
    you are consenting to your financial and personal information being passed to third party organisations necessary to process transactions such as banks, credit card companies, and the delivery services that will ship your ticket on our behalf.</p>
    </div>
</section>
<div class="clear"> </div>
<br />
	<?php echo getFooter();?>