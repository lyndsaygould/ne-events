<?php
    require_once ('functions.php');
    echo getHeader("Search Events");
?>
</header>


<section>

    <h3>Advanced Search</h3>

    <form id="searchEvents" action="searchEvents.php" method="get">
        Title of the event
        <input maxlength="256" name="searchTitle" type="search" placeholder="Search..." /><br>
        Date <!-- selecting date -->
        <?php echo "<input type=date name='date' id='date' />"; ?><br>
        Venue
        <select name="selectVenue"> <!-- selecting venue: user chooses a venue, and value (how it is named in the database) is sent -->
            <option value="">None</option>
            <?php
            // function connecting to the db
            include 'database_conn.php';

            $sqlVenue = "SELECT DISTINCT venueName FROM cte_events ORDER BY venueName";
            $rVenue = mysqli_query($conn, $sqlVenue) or die(mysqli_error($conn));

            while ($row = mysqli_fetch_assoc($rVenue)) { // loop to retrieve needed data
                    echo "<option value=\"{$row['venueName']}\">";
                    echo $row['venueName'];
                    echo "</option>\n";
            }
            mysqli_free_result($rVenue); // frees the memory associated with a result
            ?>
        </select><br>

        Location
        <select name="location"> <!-- selecting venue: user chooses a venue, and value (how it is named in the database) is sent -->
            <option value="">None</option>
            <?php
            // function connecting to the db
            include 'database_conn.php';

            $sqlLocation = "SELECT DISTINCT location FROM cte_events ORDER BY location";
            $rLocation = mysqli_query($conn, $sqlLocation) or die(mysqli_error($conn));

            while ($row = mysqli_fetch_assoc($rLocation)) { // loop to retrieve needed data
                echo "<option value=\"{$row['location']}\">";
                echo $row['location'];
                echo "</option>\n";
            }
            mysqli_free_result($rLocation); // frees the memory associated with a result
            ?>
        </select><br>
        Event Price (events that costs that or less will be listed) <!-- typing price -->
        <input name="selectPrice" type="search" placeholder="Price..." /><br>
        <input type="submit" value="Search!" /> <!-- submits the results -->
    </form>
</section>
</body>
</html>