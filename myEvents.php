<?php
    require_once ('functions.php');
    echo getHeader("My Events");
?>

<section class="wrapper margin-top-two">
    <div id="page">
	    <h2>My Events</h2>
		<p>Events you are registered to.</p>

	<div class="wrapper grid">

    <?php

        if (isset($_SESSION['logged-in'])) {
            if ($_SESSION['logged-in']) { // if it is true
                $username = $_SESSION['uName'];
            }
        } else {
            header("Location: index.php"); // redirects to homepage
            exit; // exits
        }

        include 'database_conn.php'; // makes a db connection

        $eventIDreceived = isset($_REQUEST['eventID']) ? $_REQUEST['eventID'] : null;
        $userIDreceived = isset($_REQUEST['userID']) ? $_REQUEST['userID'] : null;
        $ticketsBooked = isset($_REQUEST['tickets']) ? $_REQUEST['tickets'] : null;

        if ((!empty($eventIDreceived)) && (!empty($userIDreceived) && (!empty($ticketsBooked)))) {

            $sqlTickets = "SELECT cte_events.numberOfTickets FROM cte_events WHERE cte_events.eventID= '$eventIDreceived'";
            $rEvents = mysqli_query($conn, $sqlTickets) or die (mysqli_error($conn));
            $runTheSql = mysqli_fetch_assoc($rEvents);
            $tickets = $runTheSql['numberOfTickets'];
            mysqli_free_result($rEvents);

            $tickets = $tickets + $ticketsBooked;

            $updateTickets = "UPDATE cte_events
                      SET numberOfTickets='$tickets' 
                      WHERE eventID = '$eventIDreceived'";

            mysqli_query($conn, $updateTickets) or die (mysqli_error($conn));


            $sql = "DELETE FROM cte_registrants WHERE cte_registrants.eventID='$eventIDreceived' AND cte_registrants.registrantID='$userIDreceived'";
            echo "</div>";
            if (mysqli_query($conn, $sql)){

                echo "<p> You have successfully cancelled your registration.</p>";
            }
            else {
                echo "Error cancelling .";
            }

            mysqli_query($conn, $sql) or die(mysqli_error($conn));

            header("location: myEvents.php");
            exit;
        }

    echo "<div class=\"wrapper grid\">";
        $sql = "SELECT cte_users.userID, cte_events.eventID, cte_events.eventTitle, cte_events.eventDate, 
        cte_registrants.numberOfTickets, cte_events.eventImage
        FROM cte_events 
            JOIN cte_registrants
        ON cte_events.eventID = cte_registrants.eventID
        JOIN cte_users ON cte_users.userID = cte_registrants.registrantID 
        WHERE cte_users.username='$username'";

        $rEvents = mysqli_query($conn, $sql) or die (mysqli_error($conn)); // run the query or die if there is an error
;
        if ((mysqli_num_rows($rEvents) ==0))  {
            echo "<div style=\"text-align:center\"> <h1>You currently have no events :( </h1></div> ";
        } else {

            while ($row = mysqli_fetch_assoc($rEvents)) { // loop to retrieve needed data

                $userID = $row['userID'];
                $eID = $row['eventID'];
                $eTitle = $row['eventTitle'];
                $eDate = $row['eventDate'];
				$path = $row['eventImage'];
                $tickets = $row['numberOfTickets'];

                $today = date("Y-m-d");

                if ($eDate < $today) {
                    echo "
				 
					<div class=\"eventbox\">
					<img src=\"media/$path\"/>
						<div class=\"text-padding\">
							<h1 class=\"date margin-top-zero margin-bottom-zero\">$eDate</h1>
							<h1 class=\"margin-top-zero\"><a href=\"selectedEvent.php?eventID=$eID\">$eTitle</a></h1>
							<p>This is a past event. &nbsp <a href=\"#\">Remove Event</a></p>
						</div>
					</div>";
                } else {

                echo "
				 
					<div class=\"eventbox\">
					<img src=\"media/$path\"/>
						<div class=\"text-padding\">
							<h1 class=\"date margin-top-zero margin-bottom-zero\">$eDate</h1>
							<h1 class=\"margin-top-zero\"><a href=\"selectedEvent.php?eventID=$eID\">$eTitle</a></h1>
							<p><a href=\"myEvents.php?eventID=$eID&userID=$userID&tickets=$tickets\" onclick=\"return confirm('Are you sure you want to cancel your registration?');\">Cancel?</a> &nbsp &nbsp
							<a href=\"myEvents.php\"> Edit</a></p>
						</div>
					</div>";
                }
            }

            mysqli_free_result($rEvents); // frees the memory associated with a result
            mysqli_close($conn); // closes the database

        }
        ?>
    </div>
    </div>
</section>

<div class="clear"></div>
<br />
	<?php echo getFooter();?>