<?php
    require_once ('functions.php');
    echo getHeader("NE Events and Conferences");
    ?>

		<div class="bannercontainer">
			<img class="banner" src="images/banner2.png" />
		</div>
		
		<section>
			<div id="home-search">
			<form id="searchEvents" action="searchEvents.php" method="get">
			<input name="searchTitle" class="search-bar" type="search" placeholder="Search..." />
			<input type="submit" class="search-button" value="Go" /> <!-- submits the results -->
			</form>
			</div>
		</section>
				
				<section class="light-bg">
				<h1 class="margin-top-zero">Upcoming Events</h1>
				<div class="wrapper grid">
				
	<?php

    // function connecting to the db
    include 'database_conn.php';

    $sqlEvents = "SELECT cte_events.eventID, cte_events.eventTitle, cte_events.venueName, 
        cte_events.location, cte_events.eventDate, cte_events.eventPrice, cte_events.eventImage, cte_events.numberOfTickets, LEFT(cte_events.eventDesc, 120)
        FROM cte_events WHERE 1 LIMIT 8"; // retrieves all data ('WHERE 1') from three tables - te_events, te_category, te_venue by joining them

    $rEvents = mysqli_query($conn, $sqlEvents) or die(mysqli_error($conn)); // run the query or die if there is an error

    while ($row = mysqli_fetch_assoc($rEvents)) { // loop to retrieve data

		
		$eID = $row['eventID']; // gets event ID
            $eTitle = $row['eventTitle'];
            $eDate = $row['eventDate'];
			$eDesc = $row['LEFT(cte_events.eventDesc, 120)'];
			$ePrice = $row['eventPrice'];
            $loc = $row['location'];
            $eTickets = $row['numberOfTickets'];
			$path = $row['eventImage'];
            $today = date("Y-m-d");

        if ($eDate > $today) {
            echo "
			
			<div class=\"eventbox\">
				<a href='selectedEvent.php?eventID=$eID'>	
						<img src=\"media/$path\">
							<div class=\"price\">";

            if ($ePrice != 0){
                echo"<p>&pound;$ePrice</p>";
            } else {
                echo"<p>FREE</p>";
            }
            echo"
						</div>
						<div class=\"text-padding\">
							<h1 class=\"date margin-top-zero margin-bottom-zero\">$eDate</h1>
							<h1 class=\"margin-top-zero\">$eTitle</h1>
							<p>";

            if (strlen($eDesc) > 40){
                echo "$eDesc...";
            } else {
                echo "$eDesc";
            }
            echo"</p>
						</div>
						</a>
			</div>
			";
        }

        }

    mysqli_close($conn);


?>

				</div>
					</section>
				

					<div class="tyne-bg">
					<img src="images/tynebridge.png">
					</div>
					
					<div class="light-bg">
						<section class="wrapper">
							<h1 class="margin-top-zero">Our Vision</h1>
							<p class="center larger">To increase the global image of the north east of England, showcasing the cultural
                                diversity of the region, the industry/expertise on offer and increasing awareness of business
                                opportunities for the region. </p>
						</section>
					</div>
					<div class="clear"></div>
	<?php echo getFooter();?>



