<?php
    require_once ('functions.php');
    echo getHeader("NE Events");
?>

<section class="selected-event wrapper margin-top-two">


<?php

        // --------  DISPLAY INFO ABOUT THE EVENT ------------------


    include 'database_conn.php'; // makes a db connection

    $eID = isset($_REQUEST['eventID']) ? $_REQUEST['eventID'] : null;
    $ticketsAsked = isset($_REQUEST['tickets']) ? $_REQUEST['tickets'] : null;
    $feedbackReceived = isset($_REQUEST['feedback']) ? $_REQUEST['feedback'] : null;
    $feedbackID = isset($_REQUEST['fID']) ? $_REQUEST['fID'] : null;

    $alreadyRegistered = false;

    if (!empty($feedbackID)) {

        $sqlSet = "SET foreign_key_checks = 0";
        $rSet = mysqli_query($conn, $sqlSet) or die (mysqli_error($conn)); // run the query or die if there is an error

        $sqlDelete = "DELETE FROM cte_feedbacks WHERE feedbackID='$feedbackID'";

        if (mysqli_query($conn, $sqlDelete)){
        }else {
            echo "";
        }

        mysqli_query($conn, $sqlDelete) or die(mysqli_error($conn));


        $sqlSecondSet = "SET foreign_key_checks = 1";
        $rSecondSet = mysqli_query($conn, $sqlSecondSet) or die (mysqli_error($conn)); // run the query or die if there is an error

        header("location: selectedEvent.php?eventID=$eID");
        exit;

    }

    if (!empty($feedbackReceived)) {
        $userID = $_GET['userID'];
        $eID = $_GET['eventID'];
        $rating = $_GET['rating'];
        $feedback = $_GET['feedback'];

        $today = date("Y-m-d");

        $sqlF = "INSERT INTO cte_feedbacks (eID, uID, date, feedback, rating) values('$eID', '$userID', '$today', '$feedback', '$rating') ";

        echo "<p> Your feedback has now been submitted.</p> <br>";

        mysqli_query($conn, $sqlF) or die (mysqli_error($conn));

        header("location: selectedEvent.php?eventID=$eID");
        exit;

    }

    if (!empty($ticketsAsked)) {
        //echo "tickets: $ticketsAsked";
        if (isset($_SESSION['logged-in'])) {
            $username = $_SESSION['uName'];
            $sqlUser = "SELECT cte_users.userID FROM cte_users WHERE cte_users.username = '$username'";
            $rUser = mysqli_query($conn, $sqlUser) or die (mysqli_error($conn));
            $userIDrow = mysqli_fetch_assoc($rUser);
            $userIDFromDB = $userIDrow['userID'];
            mysqli_free_result($rUser);

            $today = date("Y-m-d");

            $updateSql = "INSERT INTO cte_registrants(eventID, registrantID, numberOfTickets, date) values('$eID', '$userIDFromDB', '$ticketsAsked', '$today') ";

            mysqli_query($conn, $updateSql) or die (mysqli_error($conn));

            $sql = "SELECT cte_events.numberOfTickets FROM cte_events WHERE cte_events.eventID= '$eID'";
            $rEvents = mysqli_query($conn, $sql) or die (mysqli_error($conn));
            $runTheSql = mysqli_fetch_assoc($rEvents);
            $tickets = $runTheSql['numberOfTickets'];
            mysqli_free_result($rEvents);

            $ticketsLeft = $tickets - $ticketsAsked;

            $updateTickets = "UPDATE cte_events
                      SET numberOfTickets='$ticketsLeft' 
                      WHERE eventID = '$eID'";

            mysqli_query($conn, $updateTickets) or die (mysqli_error($conn));

            $alreadyRegistered = true;
        }
    }

    $sql = "SELECT cte_events.eventID, cte_events.eventTitle, cte_events.eventDesc, cte_events.numberOfTickets, cte_events.video_url,
            cte_events.location, cte_events.venueName, cte_events.eventDate, cte_events.eventImage, 
            cte_events.eventFile, cte_events.file_type, cte_events.file_path , cte_events.eventPrice     
            FROM cte_events 
            WHERE cte_events.eventID = '$eID'";

    $rEvents = mysqli_query($conn, $sql) or die (mysqli_error($conn));

    $row = mysqli_fetch_assoc($rEvents);

    $eID = $row['eventID']; // gets event ID
    $eTitle = $row['eventTitle'];
    $eDate = $row['eventDate'];
    $loc = $row['location'];
    $eTickets = $row['numberOfTickets'];
    $eVenue = $row['venueName'];
    $eDesc = $row['eventDesc'];
    $path = $row['eventImage'];
    $apath = $row['eventFile'];
    $filePath = $row['file_path'];
    $price = $row['eventPrice'];
    $vidURL = $row['video_url'];
    $today = date("Y-m-d");



        echo "
    <div class=\"selected-event-wrap\">
		<div class=\"selected-event-img-wrap\"><img class=\"banner\" alt=\"event image\" src=\"media/$path\"></div>
		<div class=\"selected-event-head\"><div class=\"padding-1\">
			<h1 class=\"margin-top-zero\">$eTitle</h1>
				<p><strong>Location:</strong> $loc<br />
				<strong>Venue:</strong> $eVenue<br />
				<strong>Date:</strong> $eDate<br />
				<strong>Price:</strong> &pound;$price</p>
				<p>$eDesc</p>
	<button class=\"accordion\">View Feedback</button>
<div class=\"panel\"><div class=\"feed-scroll\">";




 // -------- FEEDBACK LIST ------------------

    $sqlGetFeedbacks = "SELECT cte_feedbacks.feedbackID, cte_feedbacks.date, cte_feedbacks.feedback, cte_feedbacks.rating, cte_users.firstName, 
                        cte_users.surName, cte_users.username FROM cte_feedbacks JOIN cte_users
                        ON cte_feedbacks.uID = cte_users.userID
                        WHERE cte_feedbacks.eID = '$eID' ORDER BY cte_feedbacks.date ";

    $rListOfFeedbcaks = mysqli_query($conn, $sqlGetFeedbacks) or die (mysqli_error($conn));

    if (!empty(mysqli_num_rows($rListOfFeedbcaks))) {

        while ($rowFeedbacks = mysqli_fetch_assoc($rListOfFeedbcaks)) {

            $fID = $rowFeedbacks['feedbackID'];
            $name = $rowFeedbacks['firstName'];
            $lastName = $rowFeedbacks['surName'];
            $date = $rowFeedbacks['date'];
            $feedback = $rowFeedbacks['feedback'];
            $rating = $rowFeedbacks['rating'];
            $uName = $rowFeedbacks['username'];



                echo "<div class=\"feedback\">
		<p class=\"date margin-bottom-zero\">$date</p>
		<h1 class=\"margin-top-zero margin-bottom-zero\">$name $lastName</h1>
		<p class=\"margin-top-zero\"><strong>Rating:</strong> $rating</p>
		<p>$feedback</p>
		<div class=\"edit-links\">
		";

                if (isset($_SESSION['logged-in'])) {
                    if ($_SESSION['logged-in']) { // if it is true
                        $username = $_SESSION['uName'];
                        if ($_SESSION['uName'] == 'nick') {
                            echo "<a href='editFeedbackChosen.php?fID=$fID'><i class=\"material-icons\">mode_edit</i></a>";
                            echo "<a href='selectedEvent.php?fID=$fID&eventID=$eID'><i class=\"material-icons\" onclick=\"return alert('The feedback has been deleted.');\">delete</i></a>";

                        } else if ($username == $uName) {
                            echo "<a href='editFeedbackChosen.php?fID=$fID'><i class=\"material-icons\">mode_edit</i></a>";
                        } else {
                            echo "<a href='reportFeedback.php?eventID=$eID&feedbackID=$fID'> Report </a>";
                        }
                    }
                }
                echo "</div></div>";
            }
        
    } else {
        echo "Sorry, no feedbacks.";
    }

 echo"</div></div>


<script>
var acc = document.getElementsByClassName(\"accordion\");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle(\"active\");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + \"px\";
    } 
  }
}
</script>
	</div>";


        if (empty($apath)) {
            echo "";
        } else {
            echo"<p> &nbsp VIEW ATTACHMENT FOR MORE INFO:  <a href=\"files/$apath\" target=\"_blank\"> $apath</a></p>";
        }

        if (empty($vidURL)) {
            echo "";
        } else {
            echo"<p><a href='$vidURL' target=\"_blank\"> &nbsp CLICK LINK TO VIEW VIDEO</a></p>";
        }

	    // --------  REGISTER TO THE EVENTS  ------------------


    if (isset($_SESSION['uName'])) {
        $username = $_SESSION['uName'];
        if ($username != 'nick') {
            if (($alreadyRegistered==true)) {
                echo "<p>You have successfully registered to the event.</p><br />";
            } else {
                $sqlToCheckIfUserIsRegistered = "SELECT cte_users.userID, cte_registrants.registrantID, cte_registrants.eventID FROM cte_users JOIN cte_registrants
                ON cte_users.userID = cte_registrants.registrantID 
                WHERE cte_users.username = '$username' AND cte_registrants.eventID = '$eID'";
                $rCheck = mysqli_query($conn, $sqlToCheckIfUserIsRegistered) or die (mysqli_error($conn));
                $row = mysqli_fetch_assoc($rCheck);

                if (mysqli_num_rows($rCheck) == 0) { //if empty
                    // echo "You have not registered to the event.</div></div>";
                    if ($eDate > $today) { //upcoming
                        if ($eTickets != 0) {
                            echo "
                                <h2>Register to event:</h2>
                                
                                <p>NUMBER OF TICKETS LEFT: $eTickets</p>
                                
                                <form id=\"register\" action=\"selectedEvent.php\" method=\"get\">
                                <p>Number of tickets I want:
                                <select name=\"tickets\">
                                    <option value=\"1\">1</option>
                                    <option value=\"2\">2</option>
                                    <option value=\"3\">3</option>
                                    <option value=\"4\">4</option>
                                    <option value=\"5\">5</option>
                                    <option value=\"6\">6</option>
                                    <option value=\"7\">7</option>
                                    <option value=\"8\">8</option>
                                    <option value=\"9\">9</option>
                                    <option value=\"10\">10</option>
                                </select></p>
                                <input type=\"hidden\" name=\"eventID\" value=$eID />
                                <input type='submit' class=\"purple-button-side\" value='Register'/>
                                </form>
                                    
                        ";
                        } else {
                            echo "<p>Sold out.</p>";
                        }
                    }
                }
				
				
				if (mysqli_num_rows($rCheck) != 0) {
                    //echo "You have registered to the event.</div></div>";
					
                    if ($eDate < $today) {
						echo"</div></div>";
                        // --------  LEAVE  FEEDBACK ------------------

                        $sqlFeedback = "SELECT cte_users.userID, cte_events.eventID
                                        FROM cte_events 
                                            JOIN cte_registrants
                                        ON cte_events.eventID = cte_registrants.eventID
                                        JOIN cte_users ON cte_users.userID = cte_registrants.registrantID 
                                        WHERE cte_users.username='$username'";

                        $rFeedback = mysqli_query($conn, $sqlFeedback) or die (mysqli_error($conn));

                        while ($row = mysqli_fetch_assoc($rFeedback)) { // loop to retrieve needed data

                            $userID = $row['userID'];
                            $eventID = $row['eventID'];

                            if ($eID == $eventID) {
                                echo "
								<section class=\"attended-event\">
								<br /><h3>Leave feedback for the event</h3>
								<form method=\"get\" action=\"selectedEvent.php\">
                            <textarea name=\"feedback\" rows=\"4\"
                                      cols=\"10\"> Write us a line or two...</textarea><br />
                            <label for=\"rating\">Rate the event:</label>
                            <input type=\"radio\" name=\"rating\" value=\"1\"> 1
                            <input type=\"radio\" name=\"rating\" value=\"2\"> 2
                            <input type=\"radio\" name=\"rating\" value=\"3\"> 3
                            <input type=\"radio\" name=\"rating\" value=\"4\"> 4
                            <input type=\"radio\" name=\"rating\" value=\"5\"> 5
                            <input type=\"radio\" name=\"rating\" value=\"6\"> 6
                            <input type=\"radio\" name=\"rating\" value=\"7\"> 7
                            <input type=\"radio\" name=\"rating\" value=\"8\"> 8
                            <input type=\"radio\" name=\"rating\" value=\"9\"> 9
                            <input type=\"radio\" name=\"rating\" value=\"10\"> 10
                            <input type='hidden' name='userID' value=$userID>
                            <input type='hidden' name='eventID' value=$eventID><br /><br />
                            <input type=\"submit\"  class=\"purple-button-side\" value=\"Submit Feedback\" onclick=\"return alert('Your feedback has been submitted.');\"/>
                        </form></section>";
                            }
                        }

                    } else if ($eDate > $today) {
                        echo "You have successfully registered to the event.<br />";
                    }
                }
            }
        }




    } else {
        if ($eTickets != 0) {
            echo "<p3>&nbsp Login and buy tickets now.</p3></div></div>";
        } else {
            echo "<p>Sold out.</p></div></div>";
        }

    }

    //mysqli_free_result($rFeedback); // frees the memory associated with a result
    //mysqli_free_result($rEvents);

    echo"
	</section><!--end selected-event-->
	
<section class=\"wrapper\">";


if ($eDate > $today) {
    echo"<br /><a class='event-link' href='events.php'>&larr; Go back to events</a>";
} else {
    echo"<br /><a class='event-link' href='pastEvents.php'>&larr; Go back to past events</a>";
}

?>
</section>
    <div class="clear"></div>
<br />
	<?php echo getFooter();?>