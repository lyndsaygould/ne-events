<?php
    require_once ('functions.php');
    echo getHeader("Register to Events");
?>

<section>

    <?php

    if (isset($_SESSION['logged-in'])) {
        $username = $_SESSION['uName'];
    }

    include 'database_conn.php'; // makes a db connection

    $eID = $_GET['eventID'];
    $noOfTickets = $_GET['tickets'];

    $sqlUser = "SELECT cte_users.userID FROM cte_users WHERE cte_users.username = '$username'";
    $rUser = mysqli_query($conn, $sqlUser) or die (mysqli_error($conn));
    $userIDrow = mysqli_fetch_assoc($rUser);
    $userIDFromDB = $userIDrow['userID'];
    mysqli_free_result($rUser);


    $today = date("Y-m-d");

    $updateSql = "INSERT INTO cte_registrants(eventID, registrantID, numberOfTickets, date) values('$eID', '$userIDFromDB', '$noOfTickets', '$today') ";

    mysqli_query($conn, $updateSql) or die (mysqli_error($conn));

    $sql = "SELECT cte_events.numberOfTickets FROM cte_events WHERE cte_events.eventID= '$eID'";
    $rEvents = mysqli_query($conn, $sql) or die (mysqli_error($conn));
    $runTheSql = mysqli_fetch_assoc($rEvents);
    $tickets = $runTheSql['numberOfTickets'];
    mysqli_free_result($rEvents);

    $ticketsLeft = $tickets - $noOfTickets;

    $updateTickets = "UPDATE cte_events
                      SET numberOfTickets='$ticketsLeft' 
                      WHERE eventID = '$eID'";

    mysqli_query($conn, $updateTickets) or die (mysqli_error($conn));
    mysqli_close($conn);


    ?>

    <br><a href="events.php">Go back to Event list</a>

</section>
</body>
</html>
