<?php
require_once ('functions.php');
echo getHeader("NE Events and Conferences");
?>

<section class="margin-top-two">

    <div id="box">
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>The results</title>
</head>
<body>

<?php
include 'database_conn.php';

$title = $_POST['title'];
$description = $_POST['eDesc'];
$tickets = $_POST['tickets'];
$location = $_POST['location'];
$venue = $_POST['venue'];
$date = $_POST['date'];
$price = $_POST['price'];
$videoURL = $_POST['vid_url'];

$filetmp = $_FILES["myFile"]{"tmp_name"};
$filename  = $_FILES["myFile"]{"name"};
$filetype  = $_FILES["myFile"]{"type"};
$filepath  = "media/" . $filename;
$imtarget_dir = "media/";
$imtarget_file = $imtarget_dir . basename($_FILES["myFile"]["name"]);
$imimageFileType = pathinfo($imtarget_file,PATHINFO_EXTENSION);


$afiletmp = $_FILES["aFile"]{"tmp_name"};
$afilename  = $_FILES["aFile"]{"name"};
$afiletype  = $_FILES["aFile"]{"type"};
$aafilepath  = "files/" . $afilename;
$target_dir = "files/";
$target_file = $target_dir . basename($_FILES["aFile"]["name"]);
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

move_uploaded_file($filetmp, $filepath);

move_uploaded_file($afiletmp, $aafilepath);


$imageValid = ($imimageFileType == "jpg" ||  $imimageFileType == "jpeg" ||
    $imimageFileType == "png");

$fileValid = ($imageFileType == "pdf" || $imageFileType == "docx" || $imageFileType == "pptx" ||
    $imageFileType == "" );

// image has to be valid to add event, every event must have an image
//If the image not valid

if (!$imageValid && !$fileValid) {
    echo "<p3>Sorry Event not added, wrong image and file type.</p3> <br/> <br/>";
    echo "<p3>Images must be JPG, JPEG and PNG files</p3> <br/> <br/>";
    echo "<p3>Files must be DOCX, PDF AND PPTX files.</p3> <br/> <br/>";
} else if (!$imageValid) {
    echo "<p3>Sorry Event not added, wrong image type.</p3> <br/> <br/>";
    echo "<p3>Images must be JPG, JPEG and PNG files</p3> <br/> <br/>";
} else if ($imageValid) {
    //add the event if image valid and no file
    if ($imageValid && empty($_FILES['aFile']['tmp_name'])) {
        $sql = "INSERT INTO cte_events (eventTitle, eventDesc, numberOfTickets, location, venueName, eventDate, eventPrice, 
                eventImage, img_path, img_type)
				values('$title', '$description', '$tickets', '$location', '$venue', '$date', '$price',
				'$filename','$filepath','$filetype') ";

        echo "<h12>New event just added</h12>  <br/> <br/>";
        echo "<p3>Title: $title</p3> <br/> <br/>";
        echo "<p3>Description:  <br/>$description</p3> <br/> <br/>";
        echo "<p3>Tickets: $tickets</p3> <br/> <br/>";
        echo "<p3>Location: $location</p3> <br/> <br/>";
        echo "<p3>Venue: $venue</p3> <br/> <br/>";
        echo "<p3>Date: $date</p3> <br/> <br/>";
        echo "<p3>Price: $price</p3> <br/> <br/>";
        echo "<img src='$filepath' id=\"eventImg\" height=\"100\" width=\"auto\"/>";

        echo " <div id=\"back\"> ";
        echo "<a class='event-link3' href='manageEvents.php'>&larr; Go back to manage events</a>";
        echo "</div>";

        mysqli_query($conn, $sql) or die (mysqli_error($conn));

    } else if ($imageValid && !empty($_FILES['aFile']['tmp_name']) && $fileValid) {
        // add event if image valid and file is not empty and file is valid
        $sql = "INSERT INTO cte_events (eventTitle, eventDesc, numberOfTickets, location, venueName, eventDate, eventPrice, 
                eventImage, img_path, img_type, 
				eventFile, file_path, file_type)
				values('$title', '$description', '$tickets', '$location', '$venue', '$date', '$price',
				'$filename','$filepath','$filetype' , 
				'$afilename' , '$aafilepath' , '$afiletype') ";

        echo "<h1>New event</h1>";

        echo "<p3>Title: $title</p3> <br/> <br/>";
        echo "<p3>Description: $description</p3> <br/> <br/>";
        echo "<p3>Tickets: $tickets</p3> <br/> <br/>";
        echo "<p3>Location: $location</p3> <br/> <br/>";
        echo "<p3>Venue: $venue</p3> <br/> <br/>";
        echo "<p3>Date: $date</p3> <br/> <br/>";
        echo "<p3>Price: $price</p3> <br/> <br/>";
        echo "<img src='$filepath' id=\"eventImg\" height=\"100\" width=\"auto\"/><br/>";
        echo "<p3> $aafilepath</p3> <br/> <br/>";

        echo " <div id=\"back\"> ";
        // open php

        echo "<a class='event-link3' href='manageEvents.php'>&larr; Go back to manage events</a>";
        echo "</div>";


        mysqli_query($conn, $sql) or die (mysqli_error($conn));

    } else if ($imageValid && !empty($_FILES['aFile']['tmp_name']) && !$fileValid) {
        // image valid and file is not empt and not valid
        // dont add event
        echo "<p3>Sorry Event not added, wrong file.</p3> <br/> <br/>";
        echo "<p3>Files must be DOCX, PDF AND PPTX files.</p3> <br/> <br/>";
    }

    if (!empty($videoURL)) {
        $sqlVid = "UPDATE cte_events SET cte_events.video_url='$videoURL' WHERE cte_events.eventTitle='$title'";
        mysqli_query($conn, $sqlVid) or die (mysqli_error($conn));

        echo "<p3>Video: $videoURL</p3> <br/> <br/>";

    }



    mysqli_close($conn);
}

?>
    </div>
</section>
<?php echo getFooter();?>
</body>
</html>
