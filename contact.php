<?php
    require_once ('functions.php');
    echo getHeader("Contact Us");
?>
	<div class="bannercontainer">
			<img class="banner" src="images/banner3.png" />
			</div>

			<div class="light-bg">	
<h1 class="margin-top-zero">Contact Us</h1>
<p class="center">Use the form below to get in touch with us and a member of our team will get in touch with you ASAP.</p><br />
<section class="contact-form-wrap">
	<form class="contact-form" name="contactform" method="post" action="email-contact-form.php">
		<p class="left-50"><label for ="first_name">First Name <span class="required">*</span></label><br />
			<input type="text" name="first_name" maxlength="50" size="30" required></p>
			
		<p class="right-50"><label for ="last_name">Surname <span class="required">*</span></label><br />
			<input type="text" name="last_name" maxlength="50" size="30" required></p>
			
		<p class="clear"><label for ="email">Email Address <span class="required">*</span></label><br />
			<input type="text" name="email" maxlength="50" size="30" required></p>
			
		<p><label for ="telephone">Telephone</label><br />
			<input type="text" name="telephone" maxlength="50" size="30"></p>
			
		<p><label for ="Subject">Subject</label><br />
			<input type="text" name="subject" maxlength="200" size="30"></p>
			
		<p><label for ="Message">Message <span class="required">*</span></label><br />
			<textarea name="message" maxlength="1000" size="30" required> </textarea></p>
			
	<input type="submit" class="purple-button-side" value="Submit">
	</form>
	
	<p class="required-msg"><br /><span class="required">*</span> = required field.</p>
	
</section><br />
</div>

<section class="dark-bg margin-top-two">
<div class="wrapper">
<h2 class="alternate">alternatively...</h2>
						<br />
						<div class="grid">
							<div class="contact-us margin-top-zero">
							<p class="icon"><i class="material-icons">drafts</i></p> 
							<h1>Post</h1>
							<p>61 Clayton Street<br />
							Newcastle, NE1 4PJ</p>
							</div>
							
							<div class="contact-us">
							<p class="icon"><i class="material-icons">call</i></p>
							<h1>Phone</h1>
							<p>Ring us: 0191 251 6666 <br />
							Text us: 07657 399 322</p>
							</div>
							
							<div class="contact-us">
							<p class="icon"><i class="material-icons">mail_outline</i></p>
							<h1>Email</h1>
							<p>hello@neevents.co.uk<br />
							We reply within 24 hours!</p>
							</div>
						</div>
</div>
</section>
	<?php echo getFooter();?>