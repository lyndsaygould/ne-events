<?php
    require_once ('functions.php');
    echo getHeader("Login");
?>
<section class="wrapper">
<h1>Login</h1>
<?php
 if (isset($_SESSION['uName'])) { // if the user logs in
        $username = $_SESSION['uName'];
        echo "Welcome, $username! <a href='logOut.php'>Logout?</a>"; // displays a welcome message and a link to log out
    } else { // if the user is not logged in
        echo '<div class="login-mobile"><form method="post" action="UserLoggedIn.php"> 
                    <label for="userName">Username:</label><br /><input type="text" name="userName"> <!-- username --><br/><br />
                    <label for="pwd">Password:</label><br /><input type="password" name="pwd"> <!-- password --><br /><br />
                    <input type="submit" class="purple-button-side" value="Login">
                </form></div>'; // displays the login form<

        if (!empty($_SESSION['errors'])) { // if the user tried to login but there are some errors
            $theErrors = $_SESSION['errors'];
            for ($a = 0; $a < count($theErrors); $a++) { // using this 'for' loop, error messages will be displayed
                echo "$theErrors[$a] <br />\n";
            }
        }
    }?>

</section>
	<br />
	<?php echo getFooter();?>