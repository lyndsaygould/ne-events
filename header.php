<?php
function getHeader()
{
	
ini_set("session.save_path", "/home/unn_w15010567/public_html/cte-DESIGN/sessionData");
session_start(); // checks the status of the session

echo"

<!DOCTYPE html>
<html lang=\"en\">
	<head>
		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>
		
		<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">
		<link href=\"https://fonts.googleapis.com/css?family=Kaushan+Script|Quicksand|Montserrat:300,400|Work+Sans\" rel=\"stylesheet\">
		<link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
		
		<title>NE Events and Conferences</title>
	</head>

	<body>
		<header>
     <div id=\"login\">";
    if (isset($_SESSION['uName'])) { // if the user logs in
        $username = $_SESSION['uName'];
        echo "<p> Welcome, $username! <a href='logOut.php'>Logout?</a></p>"; // displays a welcome message and a link to log out
    } else { // if the user is not logged in
        echo '<form method="post" action="UserLoggedIn.php"> 
                    Username:  <input type="text" name="userName"> <!-- username -->
                    Password:  <input type="password" name="pwd"> <!-- password -->
                    <input type="submit" class="button" value="Login">
                </form>'; // displays the login form

        if (!empty($_SESSION['errors'])) { // if the user tried to login but there are some errors
            $theErrors = $_SESSION['errors'];
            for ($a = 0; $a < count($theErrors); $a++) { // using this 'for' loop, error messages will be displayed
                echo "$theErrors[$a] <br />\n";
            }
        }
    }
    echo "</div>
	
				<div class=\"wrapper\">
				    <img src=\"images/logo.png\" class=\"logo\">
			
	
				
</p>


<nav>
    <ul>
        <li><a href=\"index.php\">Home</a></li>
        <li><a href=\"about.php\">About</a></li>
        <li><a href=\"events.php\">Events</a></li>
        <li><a href=\"contact.php\">Contact Us</a></li>";
    if (isset($_SESSION['uName'])) {
        $username = $_SESSION['uName'];
        if ($username != 'nick') {
            echo "<li><a href=\"myEvents.php\">My Events</a></li>";
        }
    }
    if (isset($_SESSION['uName'])) {
        $username = $_SESSION['uName'];
        if ($username == 'nick') {
            echo "<li><a href=\"manageEvents.php\">Manage Events</a></li>";
            echo "<li><a href=\"reports.php\">Reports</a></li>";
        }
    }
    echo "       
    </ul>
</nav>
			</div>
		</header>";
}