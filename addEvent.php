<?php
require_once ('functions.php');
echo getHeader("Add an Event");

if (isset($_SESSION['logged-in'])) { //checking if session set to logged in
    if ($_SESSION['logged-in']) { // if it is logged in allow them to access this page
    }
} else {
    header("Location: index.php"); // if they aren't logged in redirect to home page
    exit;
}

?>

<section class="margin-top-two">

<h1>Create an event</h1>
<p class="center">Use this form to add an event to the database.</p>
<div class="contact-form-wrap">
<form class="event-form" id="addEvent" action="addEventToDB.php"  method="post" enctype="multipart/form-data"><br />
    Event Title <input type="text" name="title" required/>  <br /> <br/>
    Description <textarea name="eDesc" required/></textarea><br /> <br/>
    Number of Tickets <input type="number" min="1" name="tickets" required/><br />  <br/>
    Location <input type="text" name="location" required/><br />  <br/>
    Venue <input type="text" name="venue" required/><br />  <br/>
    Event Date <input type="date" name="date" required/><br />  <br/>
    Price <input type="number" min="0.00" step="0.01" name="price" required/><br /> 

    <br/>
    <p> <br/> Image files muse be JPG, JPEG and PNG</p>
    <label for="file">Add Image:</label>
    <input type="file" name= "myFile" id="myFile" required/>
    <br />

    <br/>
    <br/>
    <h1> OPTIONAL FIELDS</h1>

    <p><div class="form-padding">Upload Video:</div>
    <input type="text" name="vid_url" id ="vid_url"/> <br/> <br/>


    <P> File types must be DOCX, PDF and PPTX</P>
    <div class="form-padding">Upload a file:
    <input type="file" name= "aFile" id="aFile"/> </div> <br/>




    <input type="submit" class="purple-button-side" value="Add New Event"><br />


</form>
</div>
</section><br />
<?php echo getFooter();?>