<?php
    require_once ('functions.php');
    echo getHeader("About NE Events");
?>

	<div class="bannercontainer">
			<img class="banner" src="images/aboutus-banner.png" />
			</div>

<div class="light-bg">			
<section>
<h1 class="margin-top-zero">North East Conferences and Events</h1>
<p class="center larger">NE Events is the North East's number 1 conference promotion and publication platform.</p>
</section>
</div>

<div class="dark-bg">
<section class="wrapper">
<h1>What We Do</h1>
						
						<div class="grid">
							<div class="how-it-works margin-top-zero">
							<p class="icon"><i class="material-icons">visibility</i></p> 
							<h1>Showcase</h1>
							<p>The cultural diversity and industry expertise within the region.</p>
							</div>
							
							<div class="how-it-works">
							<p class="icon"><i class="material-icons">volume_up</i></p>
							<h1>Promote</h1>
							<p>The business opportunities the region has to offer.</p>
							</div>
							
							<div class="how-it-works">
							<p class="icon"><i class="material-icons">favorite</i></p>
							<h1>Recognise</h1>
							<p>The North East by raising awareness and the global profile.</p>
							</div>
						</div>
						</div>
</section>
	<div class="margin-bottom-zero">
			<img class="banner" src="images/aboutus-banner-bottom.png" />
	</div>
	<?php echo getFooter();?>