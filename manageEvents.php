<?php
    require_once ('functions.php');
    echo getHeader("Manage Events");

	        if (isset($_SESSION['logged-in'])) {
            if ($_SESSION['logged-in']) { // if it is true
                if ($_SESSION['uName'] == 'nick')
                echo "<section class=\"wrapper margin-top-two\">

	<aside>
    <div class=\"sidesearch\">
	<h3>Search Events</h3>\n"; // welcoming message
                $username = $_SESSION['uName'];
            }
        } else {
            header("Location: index.php"); // redirects to homepage
            exit; // exits
        }
		?>



	<form id="searchManageEvents" action="searchManageEvents.php" method="get">
        
        <input maxlength="256" name="searchTitle" type="search" placeholder="Search..." /><br>
        <!-- selecting date -->
        <input type="date" name="date" id="date" /><br>
        
		
        <select name="selectVenue"> <!-- selecting venue: user chooses a venue, and value (how it is named in the database) is sent -->
            <option value="">Venue</option>
            <?php
            // function connecting to the db
            include 'database_conn.php';

            $sqlVenue = "SELECT DISTINCT venueName FROM cte_events ORDER BY venueName";
            $rVenue = mysqli_query($conn, $sqlVenue) or die(mysqli_error($conn));

            while ($row = mysqli_fetch_assoc($rVenue)) { // loop to retrieve needed data
                    echo "<option value=\"{$row['venueName']}\">";
                    echo $row['venueName'];
                    echo "</option>\n";
            }
            mysqli_free_result($rVenue); // frees the memory associated with a result
            ?>
        </select><br>

        
        <select name="location"> <!-- selecting venue: user chooses a venue, and value (how it is named in the database) is sent -->
            <option value="">Location</option>
            <?php
            // function connecting to the db
            include 'database_conn.php';

            $sqlLocation = "SELECT DISTINCT location FROM cte_events ORDER BY location";
            $rLocation = mysqli_query($conn, $sqlLocation) or die(mysqli_error($conn));

            while ($row = mysqli_fetch_assoc($rLocation)) { // loop to retrieve needed data
                echo "<option value=\"{$row['location']}\">";
                echo $row['location'];
                echo "</option>\n";
            }
            mysqli_free_result($rLocation); // frees the memory associated with a result
            ?>
        </select><br>
        <!-- typing price -->
        <input name="selectPrice" type="search" placeholder="Price" /><br>
        <input type="submit" class="purple-button-side" value="Filter" /> <!-- submits the results -->
    </form>
	</div>
	</aside>
	
    <div id="page-content">
	    <h2>Manage Events</h2>
		<p>Edit, delete or create an event.</p>
		<a class="event-link2" href="addEvent.php">Create new event &#10140;</a>
		<br /><br />
		<?php

        include 'database_conn.php'; // makes a db connection

        $eID = isset($_REQUEST['eventID']) ? $_REQUEST['eventID'] : null;

        if (!empty($eID)) {
            $sqlSet = "SET foreign_key_checks = 0";
            $rSet = mysqli_query($conn, $sqlSet) or die (mysqli_error($conn)); // run the query or die if there is an error

            $sqlDelete = "DELETE FROM cte_events WHERE eventID='$eID'";

            if (mysqli_query($conn, $sqlDelete)){
                echo "";
            }else {
                echo "oops!";
            }

            $sqlSecondSet = "SET foreign_key_checks = 1";
            $rSecondSet = mysqli_query($conn, $sqlSecondSet) or die (mysqli_error($conn)); // run the query or die if there is an error

            mysqli_query($conn, $sqlDelete) or die(mysqli_error($conn));
        }

        $sql = "SELECT cte_events.eventID, cte_events.eventTitle, cte_events.numberOfTickets, LEFT(cte_events.eventDesc, 150),
        cte_events.location, cte_events.eventDate, cte_events.eventImage, cte_events.venueName
        FROM cte_events 
        ORDER BY cte_events.eventTitle";

        $rEvents = mysqli_query($conn, $sql) or die (mysqli_error($conn)); // run the query or die if there is an error

        while ($row = mysqli_fetch_assoc($rEvents)) { // loop to retrieve needed data

            $eID = $row['eventID']; // gets event ID
            $eTitle = $row['eventTitle'];
            $eDate = $row['eventDate'];
            $loc = $row['location'];
			$venue = $row['venueName'];
			$eDesc = $row['LEFT(cte_events.eventDesc, 150)'];
            $eTickets = $row['numberOfTickets'];
			$path = $row['eventImage'];

            echo "
			 
<div class=\"upcoming-events-box\"><div class=\"padding-1\">
	<a href=\"eventDetails.php?eventID=$eID\">
		<div class=\"events-img-wrap\"><img src='media/$path'>
		<p class=\"event-date\">$eDate</p>
		</div>
	<div class=\"upcoming-events-box-text\">
	<span class=\"events-title\"><h1>$eTitle <!-- event ID (eID) is used as PK that will be used in 'selectedEvent' page--></h1></span>
		<div class=\"clear\"></div>
	<p class=\"location\">$loc / $venue</p>
	<p>";
							
							if (strlen($eDesc) > 149){
								echo "$eDesc...";
							} else {
								echo "$eDesc";
							}
							echo"</p>";
    
		if ($eTickets == 0){
								echo "<img class=\"sold-out\" src=\"images/sold-out.png\">
								<p class=\"tickets-left\">Sold out!<br /></p>
								<p class=\"hide-pc\">Sold out!</p></a>";
							} else {
								echo "<p class=\"tickets-left\">Tickets left: $eTickets</p>
								<p class=\"hide-pc\">Tickets left: $eTickets</p></a>";
							}
							echo"</p></div>
							
							<div class=\"edit-links\"><a href=\"editEventChosen.php?eventID=$eID\"><i class=\"material-icons\">mode_edit</i></a> 
	<a href=\"manageEvents.php?eventID=$eID\"  onclick=\"return confirm('Are you sure you want to delete the event');\"><i class=\"material-icons\">delete</i></a></div></p>
<div class=\"clear\"></div>
	</div></div>";

        }

        mysqli_free_result($rEvents); // frees the memory associated with a result
        mysqli_close($conn); // closes the database

        ?>

        <body/>
        </html>


	</div>
</section>
<div class="clear"> </div>
<br />
	<?php echo getFooter();?>