<head>
    <meta charset="UTF-8">
    <title> Home </title> <!-- title of the page shown in tab-->
    <link rel="stylesheet" type="text/css" href="style2.css?<?php echo time(); ?>" /> <!-- connecting the page to the stylesheet-->
</head>




<nav>
    <a href="./"><img src="images/logo.png" alt="logo" class="logo"></a>


        <ul class="topnav" id="myTopnav">
        <li></li>
        <li><a href=\"index.php\">Home</a></li>
        <li><a href=\"about.php\">About</a></li>
        <li><a href=\"events.php\">Events</a></li>
        <li><a href=\"contact.php\">Contact Us</a></li>";
        <?php
        if (isset($_SESSION['uName'])) {
            $username = $_SESSION['uName'];
            if ($username != 'nick') {
                echo "<li><a href=\"myEvents.php\">My Events</a></li>";
            }
        }
        if (isset($_SESSION['uName'])) {
            $username = $_SESSION['uName'];
            if ($username == 'nick') {
                echo "<li><a href=\"manageEvents.php\">Manage Events</a></li>";
                echo "<li><a href=\"reports.php\">Reports</a></li>";
            }
        }
        ?>
        <li class="icon">
            <a id="mobileonly">Menu</a>
            <a href="javascript:void(0);" style="font-size:15px;" onclick="myFunction()">☰</a>
        </li>
    </ul>

    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
</nav>
