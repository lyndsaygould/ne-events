<?php
    require_once ('functions.php');
    echo getHeader("Report Feedback");
?>

<section class="wrapper">
<h1>Report Feedback</h1>

    <?php

    // --------  CHECK IF THE USER IS LOGGED-IN ------------------

    if (isset($_SESSION['logged-in'])) {
        if ($_SESSION['logged-in']) { // if it is true
            $username = $_SESSION['uName'];
        }
    } else {
        header("Location: index.php"); // redirects to homepage
        exit; // exits
    }
    
    // --------  REPORT ------------------
    include 'database_conn.php'; // makes a db connection

    $eID = isset($_REQUEST['eventID']) ? $_REQUEST['eventID'] : null;
    $fID = isset($_REQUEST['feedbackID']) ? $_REQUEST['feedbackID'] : null;

    $report = isset($_REQUEST['report']) ? $_REQUEST['report'] : null;
    $date = isset($_REQUEST['date']) ? $_REQUEST['date'] : null;

    if ((!empty($report)) && (!empty($date)) ) {

        $userID = isset($_REQUEST['userID']) ? $_REQUEST['userID'] : null;
        $feedbackID = isset($_REQUEST['feedID']) ? $_REQUEST['feedID'] : null;

        $sqlReport = "INSERT INTO cte_reports (uID, feedbackID, date, report) values('$userID', '$feedbackID', '$date', '$report') ";

        if (mysqli_query($conn, $sqlReport)){
            echo "<p> Your report has been sent. </p> <br>";
        } else {
            echo "Oops! Something went wrong.";
        }

        echo "<br /><a class=\"event-link\" href=\"selectedEvent.php?eventID=$eID\"/> &larr; Go back to the event</a>";

    } else {

        $sqlFeedback = "SELECT cte_feedbacks.feedbackID, cte_feedbacks.feedback
                    FROM cte_feedbacks
                    WHERE cte_feedbacks.eID = '$eID'";

        $sqlUser = "SELECT cte_users.userID
                FROM cte_users
                WHERE cte_users.username = '$username'";

        $rFeedbacks = mysqli_query($conn, $sqlFeedback) or die (mysqli_error($conn));
        $rUsers = mysqli_query($conn, $sqlUser) or die (mysqli_error($conn));

        $fRow = mysqli_fetch_assoc($rFeedbacks);
        $uRow = mysqli_fetch_assoc($rUsers);

        $feedback = $fRow['feedback'];
        $uID = $uRow['userID'];
        $today = date("Y-m-d");

        echo "FEEDBACK: <br /> $feedback <br/><br/>";

        echo "<form method=\"get\" action=\"reportFeedback.php\">
          REPORT THE FEEDBACK: <br>
          <textarea rows=\"4\" cols=\"10\" name=\"report\"> </textarea> <br>
          <input type='hidden' name='userID' value=$uID>
          <input type='hidden' name='feedID' value=$fID>
          <input type='hidden' name='date' value=$today>
          <input type='hidden' name='eventID' value=$eID>
          <input type=\"submit\" class=\"purple-button-side\" value=\"Report\" />
          </form>";



        mysqli_free_result($rFeedbacks); // frees the memory associated with a result
        mysqli_free_result($rUsers);

    }


    mysqli_close($conn);
    ?>
    </section>
	<div class="clear"></div><br />

<?php echo getFooter();?>