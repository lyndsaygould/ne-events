<?php
    require_once ('functions.php');
    echo getHeader("Updating the Database");
?>

<section class="wrapper margin-top-two">
<h1 class="margin-top-zero">Event Updated</h1>

<?php // open php
include 'database_conn.php'; // makes a db connection

if (isset($_SESSION['logged-in'])) { //checking if session set to logged in
    if ($_SESSION['logged-in']) { // if it is logged in allow them to access this page
    }
} else {
    header("Location: index.php"); // if they aren't logged in redirect to home page
    exit;
}

// access records
$eID = isset($_REQUEST['eventID']) ? $_POST['eventID'] : null;
$eTitle = isset($_REQUEST['eventTitle']) ? $_POST['eventTitle'] : null;
$eDesc = isset($_REQUEST['eventDesc']) ? $_POST['eventDesc'] : null;
$eVenue = isset($_REQUEST['venueName']) ? $_POST ['venueName']: null;
$eDate = isset($_REQUEST['eventDate']) ? $_POST ['eventDate']: null;
$loc = isset($_REQUEST['location']) ? $_POST ['location']: null;
$eTickets = isset($_REQUEST['numberOfTickets']) ? $_POST ['numberOfTickets']: null;

$imgPath = isset($_REQUEST ['img_path']) ? $_POST ['img_path']: null ;
$filetmp = isset($_REQUEST ['myFile']['tmp_name']) ? $_POST ['myFile']['tmp_name']: null ;
$filename  = isset($_REQUEST ['myFile'] ['name']) ? $_POST ['myFile'] ['name']: null ;
$filetype  = isset($_REQUEST ['myFile'] ['type']) ? $_POST ['myFile'] ['type']: null ;
$filepath  = "media/" . $filename;

$afilePath = isset($_REQUEST ['file_path']) ? $_POST ['file_path']: null ;
$afiletmp = isset($_REQUEST ['aFile']['tmp_name']) ? $_POST ['aFile']['tmp_name']: null ;
$afilename  = isset($_REQUEST ['aFile'] ['name']) ? $_POST ['aFile'] ['name']: null ;
$afiletype  = isset($_REQUEST ['aFile'] ['type']) ? $_POST ['aFile'] ['type']: null ;
$aafilepath  = "files/" . $afilename;

$vidFile = isset($_REQUEST ['vid_url']) ? $_POST ['vid_url']: null ;

// an array
$errors = array();


if (empty($eTitle)) { // if title is empty
    $errors[] = "<p>You must have a title</p>"; // display message
} elseif (strlen($eTitle) > 100 ) { // esle if title longer than 100 characters
    $errors[] = "<p>Your title is too long</p>"; // display message
}

if (empty($eDesc)){ // description empty
    $errors[] = "<p>You must add a description</p>"; // display message
} elseif (strlen($eDesc) > 2000 ) { // if description longer than 260
    $errors[] = "<p>Your description is too long</p>"; // display message
}

if (empty($eVenue)){ // description empty
    $errors[] = "<p>You must add a venue</p>"; // display message
} elseif (strlen($eVenue) > 100 ) { // if description longer than 260
    $errors[] = "<p>Your venue name is too long</p>"; // display message
}


if (empty($loc)){ // description empty
    $errors[] = "<p>You must add a location</p>"; // display message
} elseif (strlen($loc) > 50 ) { // if description longer than 260
    $errors[] = "<p>Your location name is too long</p>"; // display message
}

if (!empty($errors)) { // if there's errors
    //message;
    echo "<p><em>The following problem(s)...</em></p>\n"; // display following errors
    for ($a=0; $a < count($errors); $a++) {
        echo "$errors[$a] <br/>\n";
    }
} else { // or else if there aren't errors
    $eTitle = filter_var($eTitle, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES); // sanitise
    $eTitle = filter_var($eTitle, FILTER_SANITIZE_SPECIAL_CHARS);

    $eDesc = filter_var($eDesc, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $eDesc = filter_var($eDesc, FILTER_SANITIZE_SPECIAL_CHARS);

    //finding out the venueID from the venueName the user selects.
    $sql = "Select eventID from cte_events where eventID = '$eID'";
    // execute
    $rsEvents = mysqli_query($conn, $sql) or die(mysqli_error($conn));
    $row = mysqli_fetch_assoc($rsEvents);
    //venueID equals the venueID in the table
    $eID = $row['eventID'];

    $filetmp = $_FILES["myFile"]{"tmp_name"};
    $filename  = $_FILES["myFile"]{"name"};
    $filetype  = $_FILES["myFile"]{"type"};
    $filepath  = "media/" . $filename;
    $imtarget_dir = "media/";
    $imtarget_file = $imtarget_dir . basename($_FILES["myFile"]["name"]);
    $imimageFileType = pathinfo($imtarget_file,PATHINFO_EXTENSION);

    $afiletmp = $_FILES["aFile"]{"tmp_name"};
    $afilename  = $_FILES["aFile"]{"name"};
    $afiletype  = $_FILES["aFile"]{"type"};
    $aafilepath  = "files/" . $afilename;
    $target_dir = "files/";
    $target_file = $target_dir . basename($_FILES["aFile"]["name"]);
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    move_uploaded_file($filetmp, $filepath);

    move_uploaded_file($afiletmp, $aafilepath);
    // update the events table.
    //set event title to the title the user changes it to
    //set event description the what the user changes it to
    // set the venueID to what the user chooses
    //set catID to what the user chooses
    // set start and end date to what user chooses
    //set price to what user chooses
    // where the even the user chose is equal to the event in the table.

// Allow certain file formats

    $imageValid = ($imimageFileType == "jpg" ||  $imimageFileType == "jpeg" || $imimageFileType == "png" || $imimageFileType == "");
    $fileValid = ($imageFileType == "pdf" || $imageFileType == "docx" || $imageFileType == "pptx" || $imageFileType == "" );

    if (empty($_FILES['myFile']['tmp_name']) && empty($_FILES['aFile']['tmp_name'])) {
        $sql = "UPDATE cte_events
            SET eventTitle='$eTitle', eventDesc='$eDesc', venueName='$eVenue', eventDate='$eDate',   location='$loc', numberOfTickets='$eTickets'
            WHERE eventID='$eID'";
            echo "<p>Event was successfully updated</p>";
    } else if (!empty($_FILES['myFile']['tmp_name']) && !empty($_FILES['aFile']['tmp_name'])){
        if (!$imageValid && !$fileValid)  {
            $sql = "UPDATE cte_events
            SET eventTitle='$eTitle', eventDesc='$eDesc', venueName='$eVenue', eventDate='$eDate',   location='$loc', numberOfTickets='$eTickets'
            WHERE eventID='$eID'";
            echo "<p>File types must be PDF, DOCX or PPTX.</p>";
            echo "<p>Image files must be JPG, JPEG or PNG. </p>";
            echo "<p>Event was updated except from image and file.</p>";
        }
        if ($imageValid & !$fileValid) {
            $sql = "UPDATE cte_events
                SET eventTitle='$eTitle', eventDesc='$eDesc', venueName='$eVenue', eventDate='$eDate',   location='$loc', numberOfTickets='$eTickets', 
				eventImage='$filename', img_path='$filepath', img_type='$filetype'
                WHERE eventID='$eID'";
           echo "<p>File types must be PDF, DOCX or PPTX.</p>";
           echo "<p>Event was updated except from image and file.</p>";
        }
        if (!$imageValid & $fileValid){
            $sql = "UPDATE cte_events
                SET eventTitle='$eTitle', eventDesc='$eDesc', venueName='$eVenue', eventDate='$eDate',   location='$loc', numberOfTickets='$eTickets', 
				eventFile='$afilename', file_path='$aafilepath', file_type='$afiletype'
                WHERE eventID='$eID'";
            echo "<p>image files must be JPG, JPEG or PNG. </p>";
           echo "<p>Event was updated except from image.</p>";
        }
        if ($imageValid & $fileValid)  {
            $sql = "UPDATE cte_events
                SET eventTitle='$eTitle', eventDesc='$eDesc', venueName='$eVenue', eventDate='$eDate',   location='$loc', numberOfTickets='$eTickets', 
				eventImage='$filename', img_path='$filepath', img_type='$filetype',
				eventFile='$afilename', file_path='$aafilepath', file_type='$afiletype'
                WHERE eventID='$eID'";
            echo "<p>Event was successfully updated.</p>";
        }
} else if  (empty($_FILES['myFile']['tmp_name']) && !empty($_FILES['aFile']['tmp_name']))  {
        if (($imageFileType != "pdf" && $imageFileType != "docx" && $imageFileType != "pptx" && $imageFileType != "" )) {
            $sql = "UPDATE cte_events
                SET eventTitle='$eTitle', eventDesc='$eDesc', venueName='$eVenue', eventDate='$eDate',   location='$loc', numberOfTickets='$eTickets'
                WHERE eventID='$eID'";
            echo "<p>file types must be pdf, docx or pptx</p>";
            echo "Event was updated except from file.";
        }else if ($imageFileType == "pdf" || $imageFileType == "docx" || $imageFileType == "pptx" || $imageFileType == "" ) {
            $sql = "UPDATE cte_events
                SET eventTitle='$eTitle', eventDesc='$eDesc', venueName='$eVenue', eventDate='$eDate',   location='$loc', numberOfTickets='$eTickets', 
				eventFile='$afilename', file_path='$aafilepath', file_type='$afiletype'
                WHERE eventID='$eID'";
            echo "<p>Event was successfully updated.</p>";
        }

    } else if (!empty($_FILES['myFile']['tmp_name']) && empty($_FILES['aFile']['tmp_name'])) {
        if ($imimageFileType != "jpg" &&  $imimageFileType != "jpeg" && $imimageFileType != "png" && $imimageFileType != "") {
            $sql = "UPDATE cte_events
                SET eventTitle='$eTitle', eventDesc='$eDesc', venueName='$eVenue', eventDate='$eDate',   location='$loc', numberOfTickets='$eTickets'
                WHERE eventID='$eID'";
            echo "<p>Image files must be JPG, JPEG or PNG. </p>";
            echo "<p>Event was updated except from image.</p>";
        }  else if ($imimageFileType = "jpg" ||  $imimageFileType = "jpeg" || $imimageFileType = "png" || $imimageFileType = "") {
            $sql = "UPDATE cte_events
                SET eventTitle='$eTitle', eventDesc='$eDesc', venueName='$eVenue', eventDate='$eDate',   location='$loc', numberOfTickets='$eTickets', 
				eventImage='$filename', img_path='$filepath', img_type='$filetype'
                WHERE eventID='$eID'";
            echo "<p>Event was successfully updated.</p>";
        }

    }

    if (!empty($vidFile)) {
        $sqlVideo = "UPDATE cte_events
                SET cte_events.video_url='$vidFile'
                WHERE cte_events.eventID='$eID'";
        mysqli_query($conn, $sqlVideo) or die(mysqli_error($conn));

    }




    ?> <!-- end php -->

<a class='event-link' href='manageEvents.php'>&larr; Go back to manage events</a>

    <?php // open php
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
    mysqli_close($conn);
}
?> <!-- end php -->

</section>
<div class="clear"></div>
<br />

	<?php echo getFooter();?>