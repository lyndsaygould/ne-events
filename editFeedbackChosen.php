
    <?php
require_once ('functions.php');
$header = getHeader("Edit Feedback");
?>

<section class="margin-top-two">

        <h1> Edit Feedback</h1> <!-- heading of box-->
		<div class="contact-form-wrap">
        <?php // open php
        if (isset($_SESSION['logged-in'])) { //checking if session set to logged in
            if (($_SESSION['logged-in'])){ // if it is logged in allow them to access this page
            }
        } else {
            header("Location: index.php"); // if they aren't logged in redirect to home page
            exit;
        }
        ?> <!-- close php -->
        <?php
        include 'database_conn.php'; // makes a db connection

        $fID = isset($_REQUEST['fID']) ? $_REQUEST['fID'] : null;
        $feedbackUpdated = isset($_REQUEST['feedback']) ? $_REQUEST['feedback'] : null;

        if (!empty($feedbackUpdated)) {
            $sqlUpdate = "UPDATE cte_feedbacks
                SET cte_feedbacks.feedback ='$feedbackUpdated'
                WHERE cte_feedbacks.feedbackID='$fID'";

            $sqlGetId = "SELECT cte_feedbacks.eID FROM cte_feedbacks WHERE cte_feedbacks.feedbackID='$fID'";
            $rID = mysqli_query($conn, $sqlGetId) or die (mysqli_error($conn));
            $IDrow = mysqli_fetch_assoc($rID);
            $eID = $IDrow['eID'];

            if (mysqli_query($conn, $sqlUpdate)){
                header("Location: selectedEvent.php?eventID=$eID");
                exit;
            } else {
                echo "Something went wrong. Go back ant try again.";
            }

        mysqli_free_result($rID);
        mysqli_query($conn, $sqlUpdate) or die(mysqli_error($conn));
        mysqli_close($conn);

        } else {

            $sql = "SELECT cte_feedbacks.feedback, cte_feedbacks.rating
            FROM cte_feedbacks 
            WHERE cte_feedbacks.feedbackID = $fID";

            $rEvents = mysqli_query($conn, $sql) or die (mysqli_error($conn));

            $row = mysqli_fetch_assoc($rEvents);

            $feedback = $row['feedback'];
            $rating = $row['rating'];

            echo "
             <form method='get' action='editFeedbackChosen.php'>
             Edit the feedback <br /><br />
             <textarea name=\"feedback\" rows=\"4\" cols=\"35\">$feedback</textarea>
             <input type=\"hidden\" name=\"fID\" value=\"$fID\" />
             <input type=\"submit\"  class=\"purple-button-side\" style=\"width: 100%!important;\" value=\"Submit!\" />
             </form>
             ";

            mysqli_free_result($rEvents);
            mysqli_close($conn);

        }


        ?>
		</div>
</section>
<div class="clear"> </div>
</p>
	<?php echo getFooter();?>