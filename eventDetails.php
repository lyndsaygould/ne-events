<?php
    require_once ('functions.php');
    echo getHeader("Selected Event");
?>
<body class="Site">
<section class="Site-content">
<div id="content1">
    
        <?php

            $eID = isset($_REQUEST['eventID']) ? $_REQUEST['eventID'] : null;
            $eTitle = isset($_REQUEST['eTitle']) ? $_REQUEST['eTitle'] : null;

            if (isset($_SESSION['logged-in'])) {
                if ($_SESSION['logged-in']) { // if it is true
                    if ($_SESSION['uName'] == 'nick')
                        echo "<h2>$eTitle details</h2>\n"; // welcoming message
                    $username = $_SESSION['uName'];
                }
            } else {
                header("Location: index.php"); // redirects to homepage
                exit; // exits
            }

            include 'database_conn.php'; // makes a db connection

            $sql = "SELECT cte_users.firstName, cte_users.surName, cte_users.email, cte_registrants.numberOfTickets
                FROM cte_users JOIN cte_registrants ON cte_registrants.registrantID = cte_users.userID
                WHERE cte_registrants.eventID = $eID";

            $sqlTickets = "SELECT cte_events.numberOfTickets
                        FROM cte_events
                        WHERE cte_events.eventID = $eID";

            $rEvents = mysqli_query($conn, $sql) or die (mysqli_error($conn));
            $rTickets = mysqli_query($conn, $sqlTickets) or die (mysqli_error($conn));

            $rowT = mysqli_fetch_assoc($rTickets);
            $ticketsLeft = $rowT['numberOfTickets'];

             if (mysqli_num_rows($rEvents) == 0) {
                         echo "<br/> No users are registered to this event";
             } else {

                 echo "Tickets left: $ticketsLeft <br/>";
                 while ($row = mysqli_fetch_assoc($rEvents)) { // loop to retrieve needed data

                $fName = $row['firstName'];
                $sName = $row['surName'];
                $email = $row['email'];
                $noOfTickets = $row['numberOfTickets'];
                echo "<br/>";

                echo "<tr>
                            <td>$fName </td> 
                            <td>$sName &nbsp  &nbsp </td>
                            <td>$email &nbsp  &nbsp </td>
                            <td>$noOfTickets</td> <br />
                     </tr>";


            }
        }
            mysqli_free_result($rEvents);
            mysqli_free_result($rTickets);
            mysqli_close($conn);

            ?>
</div>
</section>
</body>

<?php echo getFooter();?>
</html>






