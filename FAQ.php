<?php
    require_once ('functions.php');
    echo getHeader("Manage Events");
	?>

<section class="wrapper">
<h1>FAQ</h1>
    <br/> <br/>
    <div id="faq">
<p><strong>Can I purchase tickets by phone?</strong><br />
A. Yes, you can! If you have any issues feel free to get in touch.</p>

<p><strong>Q. Can I purchase tickets outside of the UK?</strong><br />
A. Purchasing tickets outside of the UK is available, but requires a UK shipping address. If you are currently outside of the UK, you can organise for your tickets to be sent to where you will be staying when you plan to attend the event you are purchasing a ticket for.</p>

<p><strong>Q. Can I cancel a ticket?</strong><br />
A. Unfortunately tickets are non-refundable and can't be cancelled or returned. The only time refunds are offered is if an event is cancelled.</p>

<p><strong>Q. What do I do if my personal details have changed since I ordered a ticket?</strong><br />
A. Please contact our team and let us know so we can update any details required on your purchase.</p>

</p>Should you have any other questions, please do not hesitate to contact us here.</p>
    </div>
</section>
<div class="clear"> </div>
<br />
	<?php echo getFooter();?>