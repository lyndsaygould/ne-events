<?php
    require_once ('functions.php');
    echo getHeader("Delete Video");
?>		
<section class="wrapper">
<h1>Edit Event</h1>

<?php

include 'database_conn.php';	  // make db connection

$eID = isset($_REQUEST['eventID']) ? $_REQUEST['eventID'] : null;

$vidFile = isset($_REQUEST['video_url']) ? $_REQUEST['video_url'] : null;


$sqlSet = "SET foreign_key_checks = 0";
$rSet = mysqli_query($conn, $sqlSet) or die (mysqli_error($conn)); // run the query or die if there is an error

$sqlDelete = "UPDATE cte_events
                SET video_url=''
                WHERE eventID='$eID'";

if (mysqli_query($conn, $sqlDelete)){
    echo "<p class=\"center\"> Your file has now been deleted.</p>";
}else {
    echo "oops!";
}

$sqlSecondSet = "SET foreign_key_checks = 1";
$rSecondSet = mysqli_query($conn, $sqlSecondSet) or die (mysqli_error($conn)); // run the query or die if there is an error

mysqli_query($conn, $sqlDelete) or die(mysqli_error($conn));
mysqli_close($conn);
?>

</section>
<div class="clear"></div>
<br />

	<?php echo getFooter();?>