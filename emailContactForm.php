<?php

if(isset($_POST['email'])) {
 
    $email_to = "maylingfall@hotmail.com";
 
    // validation expected data exists
    if(!isset($_POST['first_name']) ||
        !isset($_POST['last_name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['subject']) ||
        !isset($_POST['telephone']) ||
        !isset($_POST['message'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }



    $first_name = $_POST['first_name']; // required
    $last_name = $_POST['last_name']; // required
    $email_form = $_POST['email']; // required
    $telephone = $_POST['telephone']; // not required
    $subject = $_POST['subject']; // required
    $message = $_POST['message']; // required
 
 
    $email_message = "Someone contacted you!\n"; 
    $email_message .= "First Name: $first_name\n";
    $email_message .= "Last Name: $last_name\n";
    $email_message .= "Email: $email_form\n";
    $email_message .= "Telephone: $telephone\n";
    $email_message .= "Comments: $message\n";


    $email_form = filter_var($email_form, FILTER_SANITIZE_EMAIL);
    $telephone = filter_var($telephone, FILTER_SANITIZE_NUMBER_INT);



// create email headers
$headers = 'From: '.$email_form."\r\n".
'Reply-To: '.$email_form."\r\n" .
@mail($email_to, $subject, $email_message, $headers);  
?>
 
<!-- include your own success html here -->
 
Thank you for contacting us. We will be in touch with you very soon.
 
<?php
 
}
?>