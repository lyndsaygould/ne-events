<?php
ini_set("session.save_path", "/home/unn_w14035880/public_html/cte-DESIGN/sessionData");
session_start();

$username = filter_has_var(INPUT_POST, 'userName') ? $_POST['userName'] : null; // gets username if it is not empty
$passWord = filter_has_var(INPUT_POST, 'pwd') ? $_POST['pwd'] : null; // gets password if it is not empty

$errors = array(); // array to keep the errors, if there will be any

if (empty($username) && !empty($passWord)){ // checks if there is no username, but the password is entered
    $errors[] = "<h6>You have not entered the username</h6>"; // message is being added to array, which contains errors
} elseif (empty($passWord) && !empty($username)){ // checks if there is no password, but the username is entered
    $errors[] = "<h6>You have not entered the password</h6>"; // message is being added to array, which contains errors
} else { // if both username and password have been entered

    // sanitising the data - username
    $username = filter_var($username, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $username = filter_var($username, FILTER_SANITIZE_SPECIAL_CHARS);

    // sanitising the data - password
    $passWord = filter_var($passWord, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
    $passWord = filter_var($passWord, FILTER_SANITIZE_SPECIAL_CHARS);

    // function connecting to the db
    include 'database_conn.php'; // makes a db connection

    $sql = "SELECT passwordHash FROM cte_users WHERE username =?"; // creates a new password hash
    $stmt = mysqli_prepare($conn, $sql);    // prepare the sql statement

    mysqli_stmt_bind_param($stmt, "s", $username); // binds the parameter
    mysqli_stmt_execute($stmt);    // execute the query

    mysqli_stmt_bind_result($stmt, $passWDHash); // binds parameter to be prepared for result

    if (mysqli_stmt_fetch($stmt)) { // checks if the username is correct
        if (password_verify($passWord, $passWDHash)) { // if the password is correct
            $_SESSION['uName'] = $username; // username value is sent to session
            $_SESSION['logged-in'] = true;  // logged-in is set to true
        } else {
            $errors[] = "<h6>Password incorrect.</h6>\n"; // error message is added to the array, containing error messages
        }
    }

}

$_SESSION['errors'] = $errors ; // errors are sent to session

$referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'events.php'; // makes sure user stays in the same page, when trying to login
header("Location: $referrer");
//45$Qr87$482d