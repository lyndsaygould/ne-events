
    <?php
require_once ('functions.php');
$header = getHeader("Edit Event");
?>

<section class="margin-top-two">

        <h1>Edit Event</h1> <!-- heading of box-->
		<div class="contact-form-wrap">
        <?php // open php
        if (isset($_SESSION['logged-in'])) { //checking if session set to logged in
            if ($_SESSION['logged-in']) { // if it is logged in allow them to access this page
            }
        } else {
            header("Location: index.php"); // if they aren't logged in redirect to home page
            exit;
        }
        ?> <!-- close php -->
        <?php
        include 'database_conn.php'; // makes a db connection

        $eID = isset($_REQUEST['eventID']) ? $_REQUEST['eventID'] : null;

        $sql = "SELECT cte_events.eventID, cte_events.eventTitle, cte_events.eventDesc, cte_events.numberOfTickets, 
            cte_events.location, cte_events.venueName, cte_events.eventDate, cte_events.eventPrice, cte_events.img_path, cte_events.file_path, cte_events.eventFile, video_url
            FROM cte_events 
            WHERE cte_events.eventID = $eID";

        $rEvents = mysqli_query($conn, $sql) or die (mysqli_error($conn));

        $row = mysqli_fetch_assoc($rEvents);

        $eID = $row['eventID']; // gets event ID
        $eTitle = $row['eventTitle']; //
        $eDate = $row['eventDate']; //
        $loc = $row['location']; //
        $eTickets = $row['numberOfTickets']; //
        $eVenue = $row['venueName']; //
        $eDesc = $row['eventDesc']; //
        $price = $row['eventPrice'];
        $imgPath = $row['img_path'];
        $afilePath = $row['file_path'];
        $eFile = $row['eventFile'];
        $vidFile = $row['video_url'];


        mysqli_free_result($rEvents);
         mysqli_close($conn);

        echo"
		<p class=\"center\">You are editing the event <strong>$eTitle</strong>.</p>";
		?>
        <form id="styling2" class="event-form" action="updated.php" method="post" enctype="multipart/form-data"> <!-- form for editing event-->
            <?php echo "<input type=\"hidden\" name=\"eventID\" id =\"eventID\" value=\"$eID\" size=\"2\" readonly />"; ?>
            <p><div class="form-padding">Title</div> <?php echo "<input type=\"text\" name=\"eventTitle\" id =\"eventTitle\" value=\"$eTitle\" size=\"100\" required >"; ?>  </p> <!-- showing the event ID  read only, user cannot edit-->
            <p><div class="form-padding">Description</div> <textarea id="eventDesc" rows="4" cols="50" name="eventDesc" required><?php if ($row['eventDesc'] == $eDesc) {
                                                                                                                                            echo $row['eventDesc'];
                                                                                                                                    }
                                                                                                                            ?> </textarea>  </p>
            <p><div class="form-padding">Number of Tickets</div><?php echo "<input type=\"number\" min=\"0\" name=\"numberOfTickets\" id =\"numberOfTickets\" value=\"$eTickets\" size=\"5\" />"; ?> </p> <!-- showing the description -->
            <p><div class="form-padding">Location</div> <?php echo "<input type=\"text\" name=\"location\" id =\"location\" value=\"$loc\" size=\"50\" required >"; ?>  </p>  <!-- show the title, let user change it-->
            <p><div class="form-padding">Venue</div> <?php echo "<input type=\"text\" name=\"venueName\" id =\"venueName\" value=\"$eVenue\" size=\"20\"  required/>"; ?>   </p><!-- showing the price -->
            <p><div class="form-padding">Date</div> <!-- start date-->
            <?php
            echo "<input type=\"date\" value=\"$eDate\" id=\"eventDate\" name=\"eventDate\" />";
            ?>  </p>

            <p>Price: <?php echo "<input type=\"number\" step=\"0.01\" name=\"eventPrice\" min=\"0\" id =\"eventPrice\" value=\"$price\" size=\"5\"  />"; ?></p><!-- showing the description -->

            <?php
            echo "<div class=\"clear\"></div>
			<img src='$imgPath' width='200' height='auto'/>";
            ?>

            <p><div class="form-padding">Change Image:</div>
            <?php echo "<input type=\"file\" name= \"myFile\" id=\"myFile\"/>" ?>

            <?php
            echo "<div class=\"clear\"></div>
			<p> $eFile <br/> <br/>";

            if (empty($eFile)) {
                echo "";
            } else {
                echo "<a href='deleteFile.php?eventID=$eID' onclick=\"return confirm('Are you sure you want to remove the file?');\">Remove Current File?</a></p>";
            }

            ?>

            <p><div class="form-padding">Upload/Change file:</div>
            <?php echo "<input type=\"file\" name= \"aFile\" id=\"aFile\"/>" ?>


            <?php
            echo "<div class=\"clear\"></div>
			<p>$vidFile <br/> <br/>";

            if (empty($vidFile)) {
                echo "";
            } else {
                echo "<a href='deleteVid.php?eventID=$eID' 
                onclick=\"return confirm('Are you sure you want to remove the video?');\">Remove Current Video?</a></p>";
            }

            ?>

            <p><div class="form-padding">Upload/Change Video:</div>

            <?php echo "<input type=\"text\" name=\"vid_url\" id =\"vid_url\" value=\"$vidFile\"/>"; ?>

            <br/> <br/>
            <input type="submit" id="submit" class="purple-button-side" value="Update Event" /> <!-- submit button-->

            </select>   </p>


        </form> <!-- end of form for edit event-->
		<br />
        <a class='event-link' href='manageEvents.php'>&larr; Go back to Manage Events</a>

</div>
</section>
<div class="clear"> </div><br />
</p>
	<?php echo getFooter();?>