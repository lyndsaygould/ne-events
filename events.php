<?php
    require_once ('functions.php');
    echo getHeader("About NE Events");
?>


<section class="wrapper margin-top-two">

	<aside>
    <div class="sidesearch">
	<h3>Search Events</h3>
	<form id="searchEvents" action="searchEvents.php" method="get">
        
        <input maxlength="256" name="searchTitle" type="search" placeholder="Search..." /><br>
        <!-- selecting date -->
        <input type="date" name="date" id="date" /><br>
        
		
        <select name="selectVenue"> <!-- selecting venue: user chooses a venue, and value (how it is named in the database) is sent -->
            <option value="">Venue</option>
            <?php
            // function connecting to the db
            include 'database_conn.php';

            $sqlVenue = "SELECT DISTINCT venueName FROM cte_events ORDER BY venueName";
            $rVenue = mysqli_query($conn, $sqlVenue) or die(mysqli_error($conn));

            while ($row = mysqli_fetch_assoc($rVenue)) { // loop to retrieve needed data
                    echo "<option value=\"{$row['venueName']}\">";
                    echo $row['venueName'];
                    echo "</option>\n";
            }
            mysqli_free_result($rVenue); // frees the memory associated with a result
            ?>
        </select><br>

        
        <select name="location"> <!-- selecting venue: user chooses a venue, and value (how it is named in the database) is sent -->
            <option value="">Location</option>
            <?php
            // function connecting to the db
            include 'database_conn.php';

            $sqlLocation = "SELECT DISTINCT location FROM cte_events ORDER BY location";
            $rLocation = mysqli_query($conn, $sqlLocation) or die(mysqli_error($conn));

            while ($row = mysqli_fetch_assoc($rLocation)) { // loop to retrieve needed data
                echo "<option value=\"{$row['location']}\">";
                echo $row['location'];
                echo "</option>\n";
            }
            mysqli_free_result($rLocation); // frees the memory associated with a result
            ?>
        </select><br>
        <!-- typing price -->
        <input name="selectPrice" type="search" placeholder="Price" /><br>
        <input type="submit" class="purple-button-side" value="Filter" /> <!-- submits the results -->

    </form>
        <br /><a class="event-link right" href="pastEvents.php">&larr; Past Events</a>
    </div>
	</aside>
	
	<div id="page-content">
	    <h2>Upcoming events</h2>

<?php

    include 'database_conn.php'; // makes a db connection


    $sql = "SELECT cte_events.eventID, cte_events.eventTitle, cte_events.numberOfTickets, 
        cte_events.location, cte_events.venueName, cte_events.eventDate, cte_events.eventImage, LEFT(cte_events.eventDesc, 150)
        FROM cte_events WHERE cte_events.eventDate >= CURDATE()
        ORDER BY cte_events.eventDate ";

    $rEvents = mysqli_query($conn, $sql) or die (mysqli_error($conn)); // run the query or die if there is an error

    while ($row = mysqli_fetch_assoc($rEvents)) { // loop to retrieve needed data

        $eID = $row['eventID']; // gets event ID
        $eTitle = $row['eventTitle'];
        $eDate = $row['eventDate'];
		$eDesc = $row['LEFT(cte_events.eventDesc, 150)'];
        $loc = $row['location'];
        $eTickets = $row['numberOfTickets'];
		$path = $row['eventImage'];
        $venue = $row['venueName'];

        echo "
					
	<div class=\"upcoming-events-box\"><div class=\"padding-1\">
	<a href=\"selectedEvent.php?eventID=$eID\">
		<div class=\"events-img-wrap\"><img src='media/$path'>
		<p class=\"event-date\">$eDate</p>
		</div>
	<div class=\"upcoming-events-box-text\">
	<span class=\"events-title\"><h1>$eTitle <!-- event ID (eID) is used as PK that will be used in 'selectedEvent' page--></h1></span>
		<div class=\"clear\"></div>
	<p class=\"location\">$loc / $venue</p>
	<p>";
							
							if (strlen($eDesc) > 149){
								echo "$eDesc...";
							} else {
								echo "$eDesc";
							}
							echo"</p>";
    
		if ($eTickets == 0){
								echo "<img class=\"sold-out\" src=\"images/sold-out.png\">
								<p class=\"tickets-left\">Sold out!<br /></p>
								<p class=\"hide-pc\">Sold out!</p></a>";
							} else {
								echo "<p class=\"tickets-left\">Tickets left: $eTickets</p>
								<p class=\"hide-pc\">Tickets left: $eTickets</p></a>";
							}
							echo"</p></div>
							<div class=\"purple-button\"><a href=\"selectedEvent.php?eventID=$eID\">More information &#10140;</a></div>
							<p class=\"mobile-info\"><a class=\"hide-pc\" href=\"selectedEvent.php?eventID=$eID\">More Information...</a></p>
<div class=\"clear\"></div>
	</div></div>
			 ";

    }

    mysqli_free_result($rEvents); // frees the memory associated with a result
    mysqli_close($conn); // closes the database

?>
</div>
</section>
<div class="clear"></div>
<br />

	<?php echo getFooter();?>